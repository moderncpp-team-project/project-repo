#pragma once
#include <cstdint>
enum class MenuOption :uint8_t
{
        Menu,
        Play,
        Options,
        Exit,
        Default
};

