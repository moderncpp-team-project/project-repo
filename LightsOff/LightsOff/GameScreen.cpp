#include "GameScreen.h"
#include "Game.h";
#include "Options.h"

void GameScreen::GameScreenSetTextures(const uint16_t& WINDOW_WIDTH, const uint16_t& WINDOW_HEIGHT, ResourceLoader& resourceLoader,Options& options)
{
	background.setTexture(resourceLoader.GetTexture(ResourceLoader::TextureType::Background));
	background.setScale(1, 1);

	lightOn.setTexture(resourceLoader.GetTexture(ResourceLoader::TextureType::LightOn));
	lightOn.setScale(.2,.2);
	lightOn.setPosition(500, 215);
	lightOn.setColor(options.parseEnumColorsToSFMLColor(options.GetOnColor()));

	lightOff.setTexture(resourceLoader.GetTexture(ResourceLoader::TextureType::LightOff));
	lightOff.setScale(.1,.1);
	lightOff.setPosition(100, 100);
	lightOff.setColor(options.parseEnumColorsToSFMLColor(options.GetOffColor()));

	backButton.setTexture(resourceLoader.GetTexture(ResourceLoader::TextureType::UIButtonBack));
	backButton.setScale(.2, .2);	
	float xBackButton = WINDOW_WIDTH * 5 / 100;
	float yBackButton = WINDOW_HEIGHT * 50 / 100;
	backButton.setPosition(xBackButton, yBackButton);
	backButton.setColor(sf::Color::White);
}

void GameScreen::GameScreenDisplay(RenderWindow& gameWindow, GameScreen& gameScreen, const uint16_t& WINDOW_WIDTH, const uint16_t& WINDOW_HEIGHT, Board& board, Logger& logger, const Options& options)
{
#pragma region LoggingVariables
	static std::string BOARD_HEIGHT_LOGGING = std::to_string(options.GetHeight());
	static std::string BOARD_WIDTH_LOGGING = std::to_string(options.GetWidth());
	static std::string NEIGHBOR_RULE_LOGGING = options.GetNeighboursRules();
	static std::string TIME_ATTACK_LOGGING = options.GetTimeAttackRule();
#pragma endregion
	gameWindow.draw(gameScreen.background);
	board.SetHeightAndWidth(options.GetHeight(), options.GetWidth());
	logger.Info("The user has chosen to play a " + BOARD_HEIGHT_LOGGING + "x" + BOARD_WIDTH_LOGGING + " game with " + NEIGHBOR_RULE_LOGGING + " rule and time attack rule turned" + TIME_ATTACK_LOGGING);
	board.InitBoard();
	logger.Info("The board has been initialized with the values : " + BOARD_HEIGHT_LOGGING + " height " + BOARD_WIDTH_LOGGING + " width");
	Game game;
	game.Run(board, gameWindow, gameScreen, WINDOW_WIDTH, WINDOW_WIDTH, logger, options);
}

int GameScreen::PositionOfLightPressed(const std::vector<sf::Sprite>& lightsVector, RenderWindow& gameWindow)
{
	for (int i = 0; i < lightsVector.size(); ++i)
	{
		Vector2i mousePos = Mouse::getPosition(gameWindow);
		Vector2f mousePosAux(mousePos.x, mousePos.y);
		if (lightsVector.at(i).getGlobalBounds().contains(mousePosAux))
		{
			return i;
		}
	}
	return -1;
}

bool GameScreen::isBackButtonOrEscapePressed(RenderWindow& window)
{
	if (Mouse::isButtonPressed(Mouse::Button::Left))
	{
		Vector2i mousePos = Mouse::getPosition(window);
		Vector2f mousePosAux(mousePos.x, mousePos.y);
		if (backButton.getGlobalBounds().contains(mousePosAux))
		{
			return true;
		}
	}
	
	if (Keyboard::isKeyPressed(Keyboard::Key::Escape))
	{
		return true;
	}
	return false;
}
