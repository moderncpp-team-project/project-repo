#pragma once
#include <SFML/Graphics.hpp>
#include <cstdint>
#include <iostream>
#include "ResourceLoader.h"
#include "MenuOption.h"
#include "Logger.h"
using namespace sf;

//declarat clasa GameScreen inainte pentru a evita erorile cu headere circulare
class GameScreen;

class Options
{
public:
	enum class Colors:uint8_t
	{
		Blue, 
		Magenta, 
		Cyan,
		Yellow,		//Default On Color
		Green, 
		Red			//Default Off Color
	};
	Options();

#pragma region GetFunctions
	const bool& GetUpDown()const;
	const bool& GetLeftRight()const;
	const bool& GetMainDiagonal()const;
	const bool& GetSecundaryDiagonal()const;

	const Colors& GetOnColor()const;
	const Colors& GetOffColor()const;

	const uint16_t& GetHeight()const;
	const uint16_t& GetWidth()const;
	
	std::string GetBoardPredefinedSize() const;

	std::string GetNeighboursRules() const;
	
	std::string GetGameRandomModeRule() const;
	const bool& GetGameRandomMode() const;

	std::string GetTimeAttackRule() const;
	const bool& GetTimeAttack()const;

	const Text& GetPredefinedMap()const;
#pragma endregion


#pragma region SetFunctions
	void SetUpDown(const bool& newState);
	void SetLeftRight(const bool& newState);
	void SetMainDiagonal(const bool& newState);
	void SetSecundaryDiagonal(const bool& newState);

	void SetOnColor(const std::string& newOnColor);
	void SetOffColor(const std::string& newOffColor);

	void SetHeight(const uint16_t& newHeight);
	void SetWidth(const uint16_t& newWidth);

	void SetBoardPredefinedSizes();

	void SetPredefinedMaps();
#pragma endregion


	void UpDownOption();
	void LeftRightOption();
	void MainDiagonalOption();
	void SecundaryDiagonalOption();
	void AllNeighboursOption();
	void UpDownLeftRightOption();	
	void switchForGUI();

#pragma region ParsingFunctions
	const Colors& parseStringToColor(const std::string& toParse);
	std::string parseColorToString(const Colors toParse);

	//converteste culorile enumuli in culorile din SFML
	sf::Color parseEnumColorsToSFMLColor(Colors c);
#pragma endregion


public:
	//metoda asta afiseaza meniul de optiuni al jocului
	const MenuOption& OptionsScreenDisplay(RenderWindow& gameWindow, GameScreen& gameScreen, int WINDOW_WIDTH, int WINDOW_HEIGHT, ResourceLoader& resourceLoader, Logger& logger,Event&event);

public: 
	Sprite backButton;
	bool isBackButtonOrEscapePressed(RenderWindow& window) const;

private:
	void moveInOptionsMenu(Event&event);


private:
#pragma region BoolVariables
	bool o_upDown;
	bool o_leftRight;
	bool o_mainDiagonal;
	bool o_secundaryDiagonal;

	bool o_gameRandomMode;

	bool o_timeAttack;

	bool o_3x3;
	bool o_5x5;
	bool o_7x7;
	bool o_9x9;
#pragma endregion


#pragma region OnOffColors;
	Colors o_onColor;
	Colors o_offColor;
#pragma endregion;


#pragma region BoardSize
	uint16_t o_height;
	uint16_t o_width;
#pragma endregion;


#pragma region OptionsList
	uint16_t o_optionNumber;
	uint16_t o_optionNeighbours;
	uint16_t o_optionOnColor;
	uint16_t o_optionOffColor;
	uint16_t o_optionBoardSizes;
	std::vector<Text>o_optionsList;
	std::vector<Text>o_predefinedMaps;
	uint16_t o_optionPredefinedMapNumber;
#pragma endregion;
};