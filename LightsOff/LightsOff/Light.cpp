#include "Light.h"

Light::Light()
{
	m_state = Light::State::Off;
}

const Light::State& Light::GetState()const
{
	return m_state;
}

void Light::SetState(const Light::State& state)
{
	m_state = state;
}

void Light::Switch()
{
	if (m_state == Light::State::Off)
		m_state = Light::State::On;
	else
		m_state = Light::State::Off;
}
