#pragma once
#include<SFML/Graphics.hpp>
#include <SFML/Audio.hpp>

class ResourceLoader
{
public:
	enum class TextureType
	{
		Background,
		LightOff,
		LightOn,
		UIButtonPlay,
		UIButtonOptions,
		UIButtonExit,
		UIButtonBack
	};

public:
	bool Init();
	const sf::Texture& GetTexture(const TextureType& type) const;
	const sf::Font& GetFont() const;	
	sf::Music& GetMusicLightSwitch();

private:
	sf::Font m_textFont;	
	sf::Music m_musicLightSwitch;

	sf::Texture m_background;
	sf::Texture m_lightOff;
	sf::Texture m_lightOn;

	sf::Texture m_UIButtonPlay;
	sf::Texture m_UIButtonOptions;
	sf::Texture m_UIButtonBack;
	sf::Texture m_UIButtonExit;
};

