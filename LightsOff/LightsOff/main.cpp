#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <SFML/System.hpp>
#include "ResourceLoader.h"
#include <iostream>
#include "MenuOption.h"
#include "Screen.h"
#include "GameScreen.h"
#include "Game.h"
#include "Options.h"
#include "Logger.h"
#include "ConsoleGUI/ConsoleGUI.h"
#include <conio.h>

using namespace sf;

void GameLoop(RenderWindow& gameWindow, uint16_t WINDOW_WIDTH, uint16_t WINDOW_HEIGHT, Logger& logger);
bool InitAllScreens(RenderWindow& gameWindow, const uint16_t WINDOW_WIDTH, const uint16_t WINDOW_HEIGHT, Screen& titleScreen, ResourceLoader& resourceLoader, GameScreen& gameScreen, Options& options);

int main()
{
    static char inConsole; 

    std::cout << "Where would you like to play? \n";
    std::cout << "1. In Console \n";
    std::cout << "2. In GUI \n";
    std::cout << "Choice: ";
    inConsole=_getch();

    while (inConsole != '1' && inConsole != '2')
    {
        std::cout << "Wrong input. Try again: \n";
        inConsole = _getch();
    }

    system("cls");

    if (inConsole=='2')
    {
        Logger logger("Log.txt");

        RenderWindow gameWindow(VideoMode::getFullscreenModes()[0], "LightsOff", Style::Fullscreen);
        sf::Vector2u size = gameWindow.getSize();

        static const uint16_t WINDOW_WIDTH = size.x;
        static const uint16_t WINDOW_HEIGHT = size.y;
        gameWindow.setFramerateLimit(60);
        gameWindow.setKeyRepeatEnabled(false);

        GameLoop(gameWindow, WINDOW_WIDTH, WINDOW_HEIGHT, logger);
    }
    
    else
    {
        ConsoleGUI object;
        object.cMainMenu();
    }
    return 0;
}

void GameLoop(RenderWindow& gameWindow, uint16_t WINDOW_WIDTH, uint16_t WINDOW_HEIGHT, Logger& logger)
{
    Screen titleScreen;
    GameScreen gameScreen;
    ResourceLoader resourceLoader;
    Options options;

    Clock clock;
    Time time = clock.getElapsedTime();

    Game game;
    MenuOption whichScreen = MenuOption::Menu;

    if (InitAllScreens(gameWindow, WINDOW_WIDTH, WINDOW_HEIGHT, titleScreen, resourceLoader, gameScreen, options) == false)
    {
        return;
    }
   
    while (gameWindow.isOpen())
    {
        sf::Event event;
        while (gameWindow.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
            {
                gameWindow.close();
                logger.Info("The user closed the application succesfully!");
            }

            if (whichScreen == MenuOption::Menu)
            {
                gameWindow.clear();
                MenuOption newOption = titleScreen.MainScreenDisplay(gameWindow, titleScreen, logger);

                gameWindow.display();
                sf::sleep(sf::milliseconds(75));

                if (newOption != MenuOption::Default)
                {
                    whichScreen = newOption;
                }

            }
            else if (whichScreen == MenuOption::Play)
            {
               

                gameWindow.clear();

                Board board(options.GetWidth(), options.GetHeight());

                gameScreen.GameScreenSetTextures(WINDOW_WIDTH,WINDOW_HEIGHT,resourceLoader,options);
                gameScreen.GameScreenDisplay(gameWindow, gameScreen, WINDOW_WIDTH, WINDOW_HEIGHT, board, logger, options);

                if (game.GameSolved(board))
                {
                    whichScreen = MenuOption::Menu;
                }

                if (game.GameAborted(board))
                {
                    whichScreen = MenuOption::Menu;
                }

                gameWindow.display();
            }
            else if (whichScreen == MenuOption::Options)
            {

                gameWindow.clear();
                MenuOption newOption = options.OptionsScreenDisplay(gameWindow, gameScreen, WINDOW_WIDTH, WINDOW_HEIGHT, resourceLoader, logger, event);
                
                if (newOption != MenuOption::Default)
                {
                    whichScreen = newOption;
                }

                gameWindow.display();
            }
            else if (whichScreen == MenuOption::Exit)
            {
                gameWindow.close();
            }
        }
    }
    system("pause");
}

bool InitAllScreens(RenderWindow& gameWindow, const uint16_t WINDOW_WIDTH, const uint16_t WINDOW_HEIGHT, Screen& titleScreen, ResourceLoader& resourceLoader, GameScreen& gameScreen, Options& options)
{
    if (resourceLoader.Init())
    {
        titleScreen.MainScreenSetTextures(WINDOW_WIDTH, WINDOW_HEIGHT, resourceLoader);
        titleScreen.MainScreenSetTexts(WINDOW_WIDTH, WINDOW_HEIGHT, resourceLoader);
        gameScreen.GameScreenSetTextures(WINDOW_WIDTH, WINDOW_HEIGHT, resourceLoader,options);
        return true;
    }
    else
    {
        return false;
    }
}