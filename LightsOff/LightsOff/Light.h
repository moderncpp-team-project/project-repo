#pragma once
#include <cstdint>
#include <array>

class Light
{
public:
	enum class State :uint8_t
	{
		Off,
		On
	};
	Light();
	const State& GetState()const;
	void SetState(const State& state);
	void Switch();
private:
	State m_state;
};

