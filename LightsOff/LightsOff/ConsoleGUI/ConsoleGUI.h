#pragma once
#include "../Options.h"
class ConsoleGUI
{
public:
	void cMainMenu();

private:
	void cPlay(const Options& options);
	void cOptions(Options& options);
	void printMainMenu();
};