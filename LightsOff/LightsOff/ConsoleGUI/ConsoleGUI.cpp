#include "ConsoleGUI.h"
#include "rang.hpp"
#include <Windows.h>
#include <conio.h>
#include <fstream>
#include "../Game.h"
#include "../Logger.h"
#include "BoardConsoleFunctions.h"
#include "../dirent.h"
#include <sys/types.h>
#include <iostream>
#pragma warning(disable : 4996)
int isNewDimensionOk(const char& c);
const Options::Colors& newColor(Options& options, const std::string& stateOfLight);
void gameNormal(const Options& options);
void gameTimeAttack(const Options& options);

Logger logger("ConsoleLoggerFile.txt");
void ConsoleGUI::cMainMenu()
{
	rang::control::Force;
	std::cout << rang::fgB::cyan;
	std::string titleLightsOut = "+----------------------------------------+\n|    *Welcome to LightsOut in console*   |\n+----------------------+-----------------+\n";
	std::cout << titleLightsOut;
	printMainMenu();
	Options options;
	char choice = _getch();
	while (choice != '1' && choice != '2' && choice != '3')
	{
		system("cls");
		std::cout << "+----------------------------------------------------+\n";
		std::cout << "|  *Sorry, your choice is invalid, please try again: |\n";
		std::cout << "+----------------------------------------------------+\n";
#pragma region Logging
		std::string message = " the User has input an invalid choice in the menu screen.";
		logger.Warn(message);
#pragma endregion
		printMainMenu();
		choice = _getch();
	}
	while (choice != '3')
	{
		switch (choice)
		{
		case '1':
		{

#pragma region Logging
			std::string message = " the User has entered the Play menu screen.";
			logger.Info(message);
#pragma endregion
			cPlay(options);
			system("cls");
			std::cout << rang::fgB::cyan;
			std::cout << titleLightsOut;
			printMainMenu();
			choice = _getch();
			break;
		}
		case '2':
		{
#pragma region Logging
			std::string message = " the User has entered the Options menu screen.";
			logger.Info(message);
#pragma endregion
			cOptions(options);
			system("cls");
			std::cout << rang::fgB::cyan;
			std::cout << titleLightsOut;
			printMainMenu();
			choice = _getch();
			break;
		}
		case '3':
		{
#pragma region Logging
			std::string message = " the User has exited the console-version of the game.";
			logger.Info(message);
#pragma endregion
			break;
		}
		}
		
	}
}
void ConsoleGUI::printMainMenu()
{
	std::cout << "+----------------------+\n";
	std::cout << "|  Select your option: |\n";
	std::cout << "|  1.Play              |\n";
	std::cout << "|  2.Options           |\n";
	std::cout << "|  3.Exit              |\n";
	std::cout << "+----------------------+\n";
}

void createMap();

std::vector<std::string> showMaps();

void ConsoleGUI::cPlay(const Options& options)
{
	std::cout << rang::fgB::yellow;
	system("cls");
	std::cout << "+-----------------------------+\n";
	std::cout << "| How would you like to play? |\n";
	std::cout << "+-----------------------------+\n";
	std::cout << "| 1.Normal game               |\n";
	std::cout << "| 2.Time-Attack game          |\n";
	std::cout << "| 3.Create a new Map          |\n";
	std::cout << "| 4.Play a Predefined map     |\n";
	std::cout << "| 5.(4) but Time-Attack       |\n";
	std::cout << "| 6.Back to Main Menu...      |\n";
	std::cout << "+-----------------------------+\n";
	std::cout << "| Enter the desired game-mode |\n";
	std::cout << "+-----------------------------+\n";
	char choice = _getch();
	while (choice != '1' && choice != '2' && choice != '3' && choice != '4' && choice != '5' && choice != '6')
	{
		std::cout << "+-----------------------------+\n";
		std::cout << "| Wrong input, try again...   |\n";
		std::cout << "+-----------------------------+\n";
#pragma region Logging
		std::string message = " the User has entered an invalid input when trying to select the game-mode.";
		logger.Warn(message);
#pragma endregion
		choice = _getch();
	}
	if (choice == '1')
	{
		gameNormal(options);
		cPlay(options);
	}
	else if (choice == '2')
	{
		gameTimeAttack(options);
		cPlay(options);
	}
	else if (choice == '3')
	{
#pragma region Logging
		std::string message = " the User has entered the create map module.";
		logger.Info(message);
#pragma endregion
		createMap();
		cPlay(options);
	}
	else if (choice == '6')
	{
#pragma region Logging
		std::string message = " the User has returned to the main menu.";
		logger.Info(message);
#pragma endregion
		return;
	}
	else if (choice == '4' || choice == '5')
	{
#pragma region Logging
		{
			std::string message = " the User has entered the chose predefined map menu.";
			logger.Info(message);
		}
#pragma endregion
		system("cls");
		std::vector<std::string> maps = showMaps();
		std::string mapNumber;
		std::cin >> mapNumber;
		bool ok = false;
		while (!ok)
		{
			if (std::all_of(mapNumber.begin(), mapNumber.end(), ::isdigit))
			{
				int number = std::stoi(mapNumber);
				if (number >= 1 && number <= maps.size()+1)
					ok = true;
			}
			if (!ok)
			{
#pragma region Logging
				std::string message = " the User has entered an invalid input when trying to select a predefined map.";
				logger.Warn(message);
#pragma endregion
				std::cout << "+-----------------------------+\n";
				std::cout << "| Wrong input, try again...   |\n";
				std::cout << "+-----------------------------+\n";
				std::cin >> mapNumber;
			}
		}

		int number = std::stoi(mapNumber) - 1;
		if (number == maps.size())
		{

#pragma region Logging
			std::string message = " the User has aborted selecting a predefined map.";
			logger.Info(message);
#pragma endregion
			std::cout << "Going back.\n";
			system("pause");
			cPlay(options);
		}
		else {
			std::cout << "You've chose map: " << maps[number] << " \n";
#pragma region Logging
			std::string message = " the User has chose the [" + maps[number] + "] map.";
			logger.Info(message);
#pragma endregion
			Game game;
			if (choice == '4')
			{
#pragma region Logging
				std::string message = " the User has entered [Predefined Map : Normal Game Mode].";
				logger.Info(message);
#pragma endregion
				game.RunConsoleNormalGamePredefinedMap(maps[number],options);
			}
			else
			{
#pragma region Logging
				std::string message = " the User has entered [Predefined Map : Time-Attack Game Mode].";
				logger.Info(message);
#pragma endregion
				game.RunConsoleNormalGamePredefinedMapTimeAttack(maps[number],options);
			}
			cPlay(options);
		}
	}

}

std::vector<std::string> showMaps()
{

	std::string back = "(Not a Map) Back to Play Menu";
	struct dirent* entry;
	const char* path = "Maps/";
	DIR* dir = opendir(path);

	if (dir == NULL) {
#pragma region Logging
		std::string message = " the program has failed reading the predefined maps from the directory.";
		logger.Error(message);
#pragma endregion
		throw("Director is NULL");
	}
	std::vector<std::string> maps;
	int maxLength = 0;
	while ((entry = readdir(dir)) != NULL) 
	{
		 
		 if (entry->d_name[0] != '.')
		 {
			 maps.emplace_back(entry->d_name);
			 int length = maps.back().size();
			 if (length > maxLength)
				 maxLength = length;
		 }
	}
	std::cout << "+----------------------------------------+\n";
	std::cout << "|Choose which map you would like to play:| \n";
	std::cout << "+----------------------------------------+\n";
	
	int cnt = 0;
	if (back.size() > maxLength)
		maxLength = back.size()+1;
	maxLength = maxLength + 4;
	std::cout << "+";
	for (int i = 0; i < maxLength - 2; i++)
		std::cout << "-";
	std::cout << "+\n";
	for (auto map : maps)
	{
		std::cout <<rang::fgB::yellow<< "|" << ++cnt << "." <<rang::fgB::red <<map << rang::fgB::yellow;
		int numberOfTimes = 0;
		if (cnt <= 9)
			numberOfTimes = maxLength - map.size() - 4;
		else numberOfTimes = maxLength - map.size() - 5;
		for (int i = 0; i < numberOfTimes; i++)
			std::cout << " ";
		
		std::cout << "|\n";
	}
	//back command
	std::cout << rang::fgB::yellow << "|" << ++cnt << "." << rang::fgB::red << back << rang::fgB::yellow;
	for (int i = 0; i < maxLength - back.size() - 5; i++)
		std::cout << " ";
	std::cout << "|\n";
	//end of back command
	std::cout << "+";
	for (int i = 0; i < maxLength - 2; i++)
		std::cout << "-";
	std::cout << "+\n";
	return maps;
	closedir(dir);
}

int verifyInputPos(const std::string& position, const Board& board)
{
	for(auto l : position)
		if (!std::isdigit(l))
		{
			return -1;
		}
	int pos = std::stoi(position);
	if (pos >= 0 && pos < board.GetSize())
	{
		return pos;
	}
	return -1;
}

void createMap()
{
	Options options;
	Board board(options.GetHeight(), options.GetWidth());
	board.InitBoard();
	BoardConsoleFunctions func;
	std::string position;
	rang::fgB color = rang::fgB::gray;
	bool exit = false;
	while (!exit)
	{
		func.drawPlayScreen(board, options);
		std::cout << color << "   *Enter the position of the light which you would like to change\n";
		std::cout << "    (If you wish to abandon the creation, write 'back'): \n";
		std::cout << "    (If you wish to save the map write 'save'): ";
		std::cin >> position;
		if (position == "save")
			exit = true;
		else if (position == "back")
			exit = true;
		else {
			int pos = verifyInputPos(position, board);
			while (pos == -1)
			{
#pragma region Logging
				std::string message = " the User has a wrong input while creating a new map.";
				logger.Warn(message);
#pragma endregion
				std::cout << color << "   *Wrong input, try again...\n";
				position.clear();
				std::cin >> position;
				pos = verifyInputPos(position, board);
			}
			func.SwitchLights(pos, board, options);
		}
	}
	if (position == "save")
	{
		std::string mapName = "New User Defined Map";
		std::cout << "   *Enter your map name: ";
		std::cin >> mapName;
		std::ofstream newMap;
		newMap.open("Maps/" + mapName + ".txt");
		for (int i = 0; i < board.GetSize(); i++)
			if ((i + 1) % board.GetWidth() == 0)
			{
				newMap << static_cast<int>(board[i].GetState()) << "\n";
			}
			else
			{
				newMap << static_cast<int>(board[i].GetState()) << " ";
			}
		newMap.close();
		std::cout << "The map has been saved.";
#pragma region Logging
		std::string message = " the User has created a new map, named: " + mapName + ".";
		logger.Info(message);
#pragma endregion
	}
	else if (position == "back")
	{
		std::wcout << "Map creation aborted....\n";

#pragma region Logging
		std::string message = " the User has aborted the map creation.";
		logger.Info(message);
#pragma endregion
	}
	system("pause");
}

void gameTimeAttack(const Options& options)
{
#pragma region Logging
	std::string message = " the User has entered the game-mode: [Time-Attack].";
	logger.Warn(message);
#pragma endregion
	system("cls");
	std::cout << rang::fgB::gray << "   *In this game mode, you have 3 minutes to solve the puzzle.\n";
	std::cout << "    If you don't, you lose.\n";
	std::cout << "    Time starts when you make the 1st move.\n";
	std::cout << "    Good luck!\n";
	system("pause");
	Game game;
	game.RunConsoleTimeAttackGame(options);
}

void gameNormal(const Options& options)
{
#pragma region Logging
	std::string message = " the User has entered the game-mode: [Normal].";
	logger.Warn(message);
#pragma endregion
	Game game;
	game.RunConsoleNormalGame(options);
}

void print_Options(Options& options)
{
	auto parseBoolToOnOff = [](const bool& aux)
	{
		if (aux == true)
			return "On ";
		return "Off";
	};
	auto parseColorToString = [](const std::string& color)
	{
		if (color == "Blue")
		{
			std::cout << rang::fgB::blue << "Blue              ";
		}
		else if (color == "Cyan")
		{
			std::cout << rang::fgB::cyan << "Cyan              ";
		}
		else if (color == "Magenta")
		{
			std::cout << rang::fgB::magenta << "Magenta           ";
		}
		else if (color == "Yellow")
		{
			std::cout << rang::fgB::yellow << "Yellow            ";
		}
		else if (color == "Green")
		{
			std::cout << rang::fgB::green << "Green             ";
		}
		else {
			std::cout << rang::fgB::red << "Red               ";
		}

	};
	system("cls");
	std::cout << rang::fgB::green;
	std::cout << "+-----------------------------+\n";
	std::cout << "|          *OPTIONS*          |\n";
	std::cout << "+-----------------------------+\n";
	std::cout << "|  I.NeighboursRules:         |\n";
	std::cout << "|   1.                        |\n";
	std::cout << "|      Up and Down: " << parseBoolToOnOff(options.GetUpDown()) << "       |\n";
	std::cout << "|      Left and Right: " << parseBoolToOnOff(options.GetLeftRight()) << "    |\n";
	std::cout << "|      Main Diagonal: " << parseBoolToOnOff(options.GetMainDiagonal()) << "     |\n";
	std::cout << "|      Secundary Diagonal: " << parseBoolToOnOff(options.GetSecundaryDiagonal()) << "|\n";
	std::cout << "|  II.On/Off lights colors:   |\n";
	std::cout << rang::fgB::green << "|    2.On : ";
	parseColorToString(options.parseColorToString(options.GetOnColor()));
	std::cout << rang::fgB::green << "|\n";
	std::cout << rang::fgB::green << "|    3.Off: ";
	parseColorToString(options.parseColorToString(options.GetOffColor()));
	std::cout << rang::fgB::green << "|\n";
	std::cout << "|  III.Board Size             |\n";
	std::cout << "|    4.Width: " << options.GetWidth() << "               |\n";
	std::cout << "|    5.Height: " << options.GetHeight() << "              |\n";
	std::cout << "|    6.Predefined sizes       |\n";
	std::cout << "|  IV.Back To Main Menu       |\n";
	std::cout << "|    0.Back To Main Menu      |\n";
	std::cout << "+-----------------------------+\n";
}

void ConsoleGUI::cOptions(Options& options)
{
	print_Options(options);
	char choice = _getch();
	while (choice != '0')
	{
		switch (choice)
		{
		case '1':
		{
#pragma region Logging
			std::string message = " the User has changed the Neighbourhood state.";
			logger.Info(message);
#pragma endregion
			options.switchForGUI();
			break;
		}
		case '2':
		{
#pragma region Logging
			std::string message = " the User has changed the color on the On Light.";
		logger.Info(message);
#pragma endregion
		options.SetOnColor(options.parseColorToString(newColor(options, "On ")));
		break;
		}
		case '3':
		{
#pragma region Logging
			std::string message = " the User has changed the color on the Off Light.";
			logger.Info(message);
#pragma endregion
			options.SetOffColor(options.parseColorToString(newColor(options, "Off")));
			break;
		}
		case '4':
		{
			std::cout << "+-----------------------------+\n";
			std::cout << "|Enter the new Board Width:   |\n";
			std::cout << "|It has to be between 3 and 9 |\n";
			std::cout << "+-----------------------------+\n";
			int newWidth;
			char ch = _getch();
			newWidth = isNewDimensionOk(ch);
			while (newWidth == 0)
			{
				std::cout << "+-----------------------------+\n";
				std::cout << "|Wrong input, try again:      |\n";
				std::cout << "+-----------------------------+\n";
#pragma region Logging
				std::string message = " the User has entered and invalid input when trying to change the board Width.";
				logger.Warn(message);
#pragma endregion
				ch = _getch();
				newWidth = isNewDimensionOk(ch);
			}
#pragma region Logging
			std::string message = " the User has changed the board Width.";
			logger.Info(message);
#pragma endregion
			options.SetWidth(newWidth);
			break;
		}
		case '5':
		{

			std::cout << "+-----------------------------+\n";
			std::cout << "|Enter the new Board Height:  |\n";
			std::cout << "|It has to be between 3 and 9 |\n";
			std::cout << "+-----------------------------+\n";
			int newHeight;
			char ch = _getch();
			newHeight = isNewDimensionOk(ch);
			while (newHeight == 0)
			{
				std::cout << "+-----------------------------+\n";
				std::cout << "|Wrong input, try again:      |\n";
				std::cout << "+-----------------------------+\n";
#pragma region Logging
				std::string message = " the User has entered an invalid input when trying to change the board Height.";
				logger.Warn(message);
#pragma endregion
				ch = _getch();
				newHeight = isNewDimensionOk(ch);
			}
#pragma region Logging
			std::string message = " the User has changed the board Height.";
			logger.Warn(message);
#pragma endregion
			options.SetHeight(newHeight);
			break;
		}
		case '6':
		{
			std::cout << "+-----------------------------+\n";
			std::cout << "| The predefined size are:    |\n";
			std::cout << "| 1. 3x3                      |\n";
			std::cout << "| 2. 5x5                      |\n";
			std::cout << "| 3. 7x7                      |\n";
			std::cout << "| 4. 9x9                      |\n";
			std::cout << "+-----------------------------+\n";
			std::cout << "| Input your choice:          |\n";
			std::cout << "+-----------------------------+\n";
			char ch = _getch();
			bool ok = false;
			while (!ok)
			{
				switch (ch)
				{
				case '1':
				{
					options.SetHeight(3);
					options.SetWidth(3);
#pragma region Logging
					std::string message = " the User has channged the board size to a 3x3  board.";
					logger.Info(message);
#pragma endregion
					ok = true;
					break;
				}
				case '2':
				{
					options.SetHeight(5);
					options.SetWidth(5);
#pragma region Logging
					std::string message = " the User has channged the board size to a 5x5  board.";
					logger.Info(message);
#pragma endregion
					ok = true;
					break;
				}
				case '3':
				{
					options.SetHeight(7);
					options.SetWidth(7);
#pragma region Logging
					std::string message = " the User has channged the board size to a 7x7  board.";
					logger.Info(message);
#pragma endregion
					ok = true;
					break;
				}
				case '4':
				{
					options.SetHeight(9);
					options.SetWidth(9);
#pragma region Logging
					std::string message = " the User has channged the board size to a 9x9  board.";
					logger.Info(message);
#pragma endregion
					ok = true;
					break;
				}
				default:
				{
					std::cout << "+-----------------------------+\n";
					std::cout << "| Wrong input, try again:     |\n";
					std::cout << "+-----------------------------+\n";
#pragma region Logging
					std::string message = " the User has entered an invalid input when trying to change the board size (NxM).";
					logger.Warn(message);
#pragma endregion
					ch = _getch();
					break;
				}
				}
			}
			break;
		}
		case '0':
		{
#pragma region Logging
			std::string message = " the User has exited the options menu screen and returned to the main menu screen.";
			logger.Info(message);
#pragma endregion
			break;
		}
		default:
		{
			std::cout << "+-----------------------------+\n";
			std::cout << "| Wrong choice, try again:    |\n";
			std::cout << "+-----------------------------+\n";
#pragma region Logging
			std::string message = " the User has entered and invalid option when trying to select and option choice.";
			logger.Warn(message);
#pragma endregion
			choice = _getch();
			break;
		}
		}
		system("cls");
		print_Options(options);
		choice = _getch();
	}//end of big while
}

const Options::Colors& newColor(Options& options, const std::string& stateOfLight)
{
	system("cls");
	std::cout << "+-----------------------------+\n";
	std::cout << "| The colors you can choose   |\n";
	std::cout << "|  for the " << stateOfLight << "light  are:     |\n";
	std::cout << "+-----------------------------+\n";
	std::cout << "| 1.Blue                      |\n";
	std::cout << "| 2.Cyan                      |\n";
	std::cout << "| 3.Magenta                   |\n";
	std::cout << "| 4.Green                     |\n";
	std::cout << "| 5.Yellow                    |\n";
	std::cout << "| 6.Red                       |\n";
	std::cout << "+-----------------------------+\n";
	std::cout << "| Enter your choice:          |\n";
	std::cout << "+-----------------------------+\n";
	char choice = _getch();
	while (choice != '1' && choice != '2' && choice != '3' && choice != '4' && choice != '5' && choice != '6')
	{
		std::cout << "+-----------------------------+\n";
		std::cout << "| Wrong input, try again:     |\n";
		std::cout << "+-----------------------------+\n";
#pragma region Logging
		std::string message = " the User has entered and invalid option when trying to a new color for the ";
		message += stateOfLight;
		message += "light.";
		logger.Warn(message);
#pragma endregion
		choice = _getch();
	}
	std::string toCoutOn = "+-----------------------------+\n|The On Light can't be same   |\n| color as the Off Light.     |\n+-----------------------------+\n";
	std::string toCoutOf = "+-----------------------------+\n|The Off Light can't be same  |\n| color as the On Light.      |\n+-----------------------------+\n";

	
	switch (choice)
	{
	case '1':
	{
		if (stateOfLight == "On " && options.GetOffColor() == Options::Colors::Blue)
		{
			std::cout << toCoutOn;
			system("pause");
			return options.GetOnColor();
		}
		else if (stateOfLight == "Off" && options.GetOnColor() == Options::Colors::Blue)
		{
			std::cout << toCoutOn;
			system("pause");
			return options.GetOffColor();
		}
		else return Options::Colors::Blue;
		break;
	}
	case '2':
	{
		if (stateOfLight == "On " && options.GetOffColor() == Options::Colors::Cyan)
		{
			std::cout << toCoutOn;
			system("pause");
			return options.GetOnColor();
		}
		else if (stateOfLight == "Off" && options.GetOnColor() == Options::Colors::Cyan)
		{
			std::cout << toCoutOn;
			system("pause");
			return options.GetOffColor();
		}
		else return Options::Colors::Cyan;
		break;
	}
	case '3':
	{
		if (stateOfLight == "On " && options.GetOffColor() == Options::Colors::Magenta)
		{
			std::cout << toCoutOn;
			system("pause");
			return options.GetOnColor();
		}
		else if (stateOfLight == "Off" && options.GetOnColor() == Options::Colors::Magenta)
		{
			std::cout << toCoutOn;
			system("pause");
			return options.GetOffColor();
		}
		else return Options::Colors::Magenta;
		break;
	}
	case '4': 
	{
		if (stateOfLight == "On " && options.GetOffColor() == Options::Colors::Green)
		{
			std::cout << toCoutOn;
			system("pause");
			return options.GetOnColor();
		}
		else if (stateOfLight == "Off" && options.GetOnColor() == Options::Colors::Green)
		{
			std::cout << toCoutOn;
			system("pause");
			return options.GetOffColor();
		}
		else return Options::Colors::Green;
		break;
	}
	case '5':
	{
		if (stateOfLight == "On " && options.GetOffColor() == Options::Colors::Yellow)
		{
			std::cout << toCoutOn;
			system("pause");
			return options.GetOnColor();
		}
		else if (stateOfLight == "Off" && options.GetOnColor() == Options::Colors::Yellow)
		{
			std::cout << toCoutOn;
			system("pause");
			return options.GetOffColor();
		}
		else return Options::Colors::Yellow;
		break;
	}
	case '6':
	{
		if (stateOfLight == "On " && options.GetOffColor() == Options::Colors::Red)
		{
			std::cout << toCoutOn;
			system("pause");
			return options.GetOnColor();
		}
		else if (stateOfLight == "Off" && options.GetOnColor() == Options::Colors::Red)
		{
			std::cout << toCoutOn;
			system("pause");
			return options.GetOffColor();
		}
		else return Options::Colors::Red;
		break;
	}
	}

}

int isNewDimensionOk(const char& c)
{
	switch (c)
	{
	case '3':
		return 3;
	case '4':
		return 4;
	case '5':
		return 5;
	case '6':
		return 6;
	case '7':
		return 7;
	case '8':
		return 8;
	case '9':
		return 9;
	default:
		return 0;
	}
}