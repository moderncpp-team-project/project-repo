#include "BoardConsoleFunctions.h"
#include <Windows.h>
#include "ConsoleGUI/rang.hpp"
void BoardConsoleFunctions::SwitchLights(const int& lightNumber, Board& board, const Options& options)
{
	board[lightNumber].Switch();
	if (options.GetUpDown())
	{
		SwitchLightUp(lightNumber, board);
		SwitchLightDown(lightNumber, board);
	}
	if (options.GetLeftRight())
	{
		SwitchLightLeft(lightNumber, board);
		SwitchLightRight(lightNumber, board);
	}
	if (options.GetMainDiagonal())
	{
		SwitchLightLeftUp(lightNumber, board);
		SwitchLightRightDown(lightNumber, board);
	}
	if (options.GetSecundaryDiagonal())
	{
		SwitchLightRightUp(lightNumber, board);
		SwitchLightLeftDown(lightNumber, board);
	}
}
void drawALight(const int& position, Board& board, const Options& options);
void setColorUI(const rang::fgB color);
void BoardConsoleFunctions::drawPlayScreen(Board& board, const Options& options)
{
	system("cls");
	std::string line = "+-";
	rang::fgB color = rang::fgB::gray;
	for (int i = 0; i < options.GetWidth(); i++)
		line += "---";
	setColorUI(color);
	line += "+\n";
	std::cout << line;
	std::string lineBegging = "| ";
	std::string lineEnding = "|\n";
	for (int position = 0; position < board.GetSize(); position++)
	{
		if (position % board.GetWidth() == 0)
		{
			setColorUI(color);
			std::cout << lineBegging;
		}
		drawALight(position, board, options);
		if (position % board.GetWidth() == board.GetWidth() - 1)
		{
			setColorUI(color);
			std::cout << lineEnding;
		}
	}
	setColorUI(color);
	std::cout << line;
}
void setColorUI(const rang::fgB color)
{
	std::cout << color;
}
void setColor(const Options::Colors& color)
{
	switch (color)
	{
	case Options::Colors::Blue:
	{
		std::cout << rang::fgB::blue;
		return;
	}
	case Options::Colors::Cyan:
	{
		std::cout << rang::fgB::cyan;
		return;
	}
	case Options::Colors::Green:
	{
		std::cout << rang::fgB::green;
		return;
	}
	case Options::Colors::Magenta:
	{
		std::cout << rang::fgB::magenta;
		return;
	}
	case Options::Colors::Red:
	{
		std::cout << rang::fgB::red;
		return;
	}
	case Options::Colors::Yellow:
	{
		std::cout << rang::fgB::yellow;
		return;
	}
	}
}
void drawALight(const int& position, Board& board, const Options& options)
{
	if (board[position].GetState() == Light::State::On)
		setColor(options.GetOnColor());
	else 
		setColor(options.GetOffColor());
	if (position < 10)
		std::cout << 0;
	std::cout << position << " ";
	
}
void BoardConsoleFunctions::SwitchLightUp(const int& lightNumber, Board& board)
{
	if (lightNumber >= board.GetWidth())
	{
		board[lightNumber - board.GetWidth()].Switch();
	}
}

void BoardConsoleFunctions::SwitchLightDown(const int& lightNumber, Board& board)
{
	if (lightNumber < board.GetSize() - board.GetWidth())
	{
		board[lightNumber + board.GetHeight()].Switch();
	}
}

void BoardConsoleFunctions::SwitchLightLeft(const int& lightNumber, Board& board)
{
	if (lightNumber % board.GetWidth() != 0)
	{
		board[lightNumber - 1].Switch();
	}
}

void BoardConsoleFunctions::SwitchLightRight(const int& lightNumber, Board& board)
{
	if ((lightNumber + 1) % board.GetWidth() != 0)
	{
		board[lightNumber + 1].Switch();
	}
}

void BoardConsoleFunctions::SwitchLightLeftUp(const int& lightNumber, Board& board)
{
	if (lightNumber % board.GetWidth() != 0 && lightNumber >= board.GetWidth())
	{
		board[lightNumber - board.GetWidth() - 1].Switch();
	}
}

void BoardConsoleFunctions::SwitchLightLeftDown(const int& lightNumber, Board& board)
{
	if (lightNumber % board.GetWidth() != 0 && lightNumber < board.GetSize() - board.GetWidth())
	{
		board[lightNumber + board.GetWidth() - 1].Switch();
	}
}

void BoardConsoleFunctions::SwitchLightRightUp(const int& lightNumber, Board& board)
{
	if ((lightNumber + 1) % board.GetWidth() != 0 && lightNumber >= board.GetWidth())
	{
		board[lightNumber - board.GetWidth() + 1].Switch();
	}
}

void BoardConsoleFunctions::SwitchLightRightDown(const int& lightNumber, Board& board)
{
	if ((lightNumber + 1) % board.GetWidth() != 0 && lightNumber < board.GetSize() - board.GetWidth())
	{
		board[lightNumber + board.GetWidth() + 1].Switch();
	}
}
