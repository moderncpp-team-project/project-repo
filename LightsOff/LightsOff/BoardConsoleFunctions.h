#pragma once
#include "../LightsOff/Board.h"
#include "../LightsOff/Options.h"
class BoardConsoleFunctions
{
public:
	void SwitchLights(const int& lightNumber, Board& board, const Options& options);
	void drawPlayScreen(Board& board, const Options& options);

private:
	void SwitchLightUp(const int& lightNumber, Board& board);
	void SwitchLightDown(const int& lightNumber, Board& board);
	void SwitchLightLeft(const int& lightNumber, Board& board);
	void SwitchLightRight(const int& lightNumber, Board& board);


	void SwitchLightLeftUp(const int& lightNumber, Board& board);
	void SwitchLightLeftDown(const int& lightNumber, Board& board);

	void SwitchLightRightUp(const int& lightNumber, Board& board);
	void SwitchLightRightDown(const int& lightNumber, Board& board);
};

