#pragma once
#include<SFML/Graphics.hpp>
#include<iostream>
#include "ResourceLoader.h"
#include "GameScreen.h"
#include "MenuOption.h"
#include "Logger.h"

using namespace sf;

class Screen
{
public:
	Sprite background, playButton, exitButton, exitHover, optionsButton, optionsHover;
	Text welcomeText, playText, exitText, optionsText;

	void MainScreenSetTextures(const uint16_t& WINDOW_WIDTH, const uint16_t& WINDOW_HEIGHT, ResourceLoader& resourceLoader);
	void MainScreenSetTexts(const uint16_t& WINDOW_WIDTH, const uint16_t& WINDOW_HEIGHT, ResourceLoader& resourceLoader);
	const MenuOption& MainScreenDisplay(RenderWindow& gameWindow, Screen& object, Logger& logger);

	bool PlayButtonIsPressed(RenderWindow& window)const;
	bool OptionsButtonIsPressed(RenderWindow& window)const;
	bool ExitButtonIsPressed(RenderWindow& window)const;
	/*void menu_Select(RenderWindow& map, Screen& object, bool& selected);*/
};