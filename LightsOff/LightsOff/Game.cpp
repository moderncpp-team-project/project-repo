#include "Game.h"
#include <vector>
#include <SFML/Graphics.hpp>
#include <Windows.h>
#include <regex>
#include <iostream>
#include <fstream>
#include "ResourceLoader.h"
#include "MenuOption.h"
#include "GameScreen.h"
#include "ConsoleGUI/BoardConsoleFunctions.h"
#include "ConsoleGUI/rang.hpp"


bool checkAbort(const std::string& pos)
{
	if (pos == "exit")
		return true;
	return false;
}

void Game::initRandomBoardConsole(Board& board,const Options& options) 
{
	//+5 makes sure that, random light will be turnt at least 5 times
	int randomNumberOfTimes = rand() % board.GetLights().size() + 5;
	int contorNumberOfTimes = 0;
	int randomPosition;
	BoardConsoleFunctions func;
	while (contorNumberOfTimes < randomNumberOfTimes)
	{
		randomPosition = rand() % board.GetLights().size();
		func.SwitchLights(randomPosition,board,options);
		//the contor incrementetion
		++contorNumberOfTimes;
	}
}

int verifyInputPosition(const std::string& position, const Board& board);

void Game::RunConsoleTimeAttackGame(const Options& options)
{
	Board board(options.GetHeight(), options.GetWidth());
	board.InitBoard();
	initRandomBoardConsole(board, options);
	Time timeUntilLoss = sf::seconds(180);
	Clock clock;
	BoardConsoleFunctions func;
	std::string position;
	rang::fgB color = rang::fgB::gray;
	bool startClock = true;
	while (!GameSolved(board) && clock.getElapsedTime() <= timeUntilLoss)
	{
		func.drawPlayScreen(board, options);
		std::cout << color << "   *Enter the position of the light which you would like to change: \n";
		std::cout << color << "    (Write 'exit' if you want to abandon the game): \n";
		std::cin >> position;
		if (checkAbort(position))
		{
			std::cout << color << "    *The game has been aborted.....\n";
			system("pause");
			return;
		}
		else
		{
		if (startClock)
		{
			startClock = false;
			clock.restart();
		}
		int pos = verifyInputPosition(position, board);
		while (pos == -1)
		{
			std::cout << "pos = " << pos << " \n";
			std::cout << color << "   *Wrong input, try again...\n";
			std::cin >> position;
			if (checkAbort(position))
			{
				std::cout << color << "    *The game has been aborted.....\n";
				system("pause");
				return;
			}
			pos = verifyInputPosition(position, board);
		}
		func.SwitchLights(pos, board, options);
		}
	}
	Time time = clock.getElapsedTime();
	if(time < timeUntilLoss)
	std::cout << color << "   *Congratulations!! You finished the game in: " << time.asSeconds() << " seconds.\n";
	else
	{
		std::cout << color << "   *Sadly time's up. You lost! \n";
	}
	system("pause");
}



Game::Game()
{
	srand((unsigned int)time(NULL));
}

void Game::Run(Board& board, sf::RenderWindow& gameWindow, GameScreen& gameScreen, const uint16_t& WINDOW_WIDTH, const uint16_t& WINDOW_HEIGHT, Logger& logger,const Options& options)
{

	bool timeAttackOption = false;
	float secLimit = 60;
	float secElapsed;
	std::string remainingString;
	if (options.GetTimeAttack() == true)
	{
		timeAttackOption = true;
	}

	ResourceLoader resourceLoader;
	Clock clock;
	Time time;
	Text timeText;
	timeText.setFillColor(sf::Color(204, 204, 204));
	std::string secondsString;
	Music musicClick;
	musicClick.openFromFile("Musics/SwitchClick.wav");
	musicClick.setVolume(5);

	Font font;
	font.loadFromFile("Font/Beaufort W01 Regular.ttf");

	timeText.setFont(font);
	//float xLoadingPosTimer = WINDOW_WIDTH - timeText.getLocalBounds().width - 1850;
	float xLoadingPosTimer = WINDOW_WIDTH * 5 / 100;
	float yLoadingPosTimer = WINDOW_HEIGHT * 15 / 100;
	timeText.setPosition(xLoadingPosTimer, yLoadingPosTimer);
	timeText.setScale(1.2, 1.2);

	std::pair<sf::Sprite, sf::Sprite>OnOff = { gameScreen.lightOn,gameScreen.lightOff };
	std::vector <sf::Sprite> lightsVector;
	int initialx = gameWindow.getSize().x / 2 - board.GetWidth() * 50;
	int x = gameWindow.getSize().x / 2 - board.GetWidth() * 50;
	int y = gameWindow.getSize().y / 2 - board.GetHeight() * 50;
	int xcoef = 100;
	int ycoef = 100;


	if (options.GetGameRandomMode())
	{
		initRandomBoard(board, lightsVector, OnOff, options);
		logger.Info("Board has been initialized with random light states!");
	}
	else 
	{
		initPredefinedMap(board,options);
		logger.Info(options.GetPredefinedMap().getString() + " has been imported from the disk!");
	}

	for (Light curent : board.GetLights())
	{
		sf::Sprite newsprite;
		
		if (curent.GetState() == Light::State::Off)
			newsprite = gameScreen.lightOff;
		else newsprite = gameScreen.lightOn;
		newsprite.setPosition({ (float)x,(float)y });
		lightsVector.emplace_back(newsprite);
		if (lightsVector.size() % board.GetWidth() == 0)
		{
			x = initialx;
			y += ycoef;
		}
		else
		{
			x += xcoef;
		}
		
	}
	drawBoard(lightsVector, gameWindow);

	bool gameRunning = true;

	while (!GameSolved(board))
	{
		if (Mouse::isButtonPressed(Mouse::Button::Left))
		{

			int position = gameScreen.PositionOfLightPressed(lightsVector, gameWindow);
			if (position >= 0)
			{
				musicClick.play();
				board.SwitchLights(position, options,lightsVector,OnOff);
				logger.Info("The user has clicked on the light located at position : " + std::to_string(position));
			}				
		}

		 if (gameScreen.isBackButtonOrEscapePressed(gameWindow))
		{
			logger.Warn("The user pressed the back button or hit ESC key, aborting the game.");
			gameRunning = false;
			break;
		}

		time = clock.getElapsedTime();
		secElapsed = time.asSeconds();
		secondsString = std::to_string(time.asSeconds());
		secondsString = secondsString.substr(0, secondsString.find("."));		

		if (timeAttackOption == true)
		{
			remainingString = std::to_string((int)(secLimit - secElapsed));
			timeText.setString("Time remaining: " + remainingString);

			if (secElapsed > secLimit)
			{				
				gameRunning = false;
				break;
			}
		}
		else
		{
			timeText.setString("Elapsed time: " + secondsString);
		}

		gameWindow.clear();
		gameWindow.draw(gameScreen.background);
		gameWindow.draw(gameScreen.backButton);

		drawBoard(lightsVector, gameWindow);
		gameWindow.draw(timeText);
		gameWindow.display();
		sf::sleep(sf::milliseconds(100));
	}//end of game

	if (gameRunning == false)
	{
		GameAborted(board);		
	}
	else
	{		
		Text victoryText;
		victoryText.setFont(font);
		victoryText.setString("Congratulations! You finished the game!");
		float xVictory = WINDOW_WIDTH * 37 / 100;
		float yVictory = WINDOW_HEIGHT * 10 / 100;
		victoryText.setPosition(xVictory, yVictory);
		gameWindow.draw(victoryText);
		gameWindow.display();

		sf::sleep(sf::milliseconds(2000));

		logger.Info("The user solved the game in : " + secondsString + " seconds.");
	}
	
}

int verifyInputPosition(const std::string& position,const Board&board)
{
	std::regex number("[0-9]+");
	if (std::regex_match(position, number))
	{
		int pos = std::stoi(position);
		if (pos >= 0 && pos < board.GetSize())
		{
			return pos;
		}
	}
	return -1;
}

void Game::RunConsoleNormalGame(const Options& options)
{
	Board board(options.GetHeight(), options.GetWidth());
	board.InitBoard();
	initRandomBoardConsole(board,options);
	Time time;
	Clock clock;
	BoardConsoleFunctions func;
	std::string position;
	rang::fgB color = rang::fgB::gray;
	bool startClock = true;
	//int i = 10;
	while (!GameSolved(board))
	{
		func.drawPlayScreen(board, options);
		std::cout << color << "   *Enter the position of the light which you would like to change: \n";
		std::cout << color << "    (Write 'exit' if you want to abandon the game): \n";
		std::cin >> position;
		if (checkAbort(position))
		{
			std::cout << color << "    *The game has been aborted.....\n";
			system("pause");
			return;
		}
		else
		{
			if (startClock)
			{
				startClock = false;
				clock.restart();
			}
		int pos = verifyInputPosition(position, board);
		while (pos == -1)
		{
			std::cout << "pos = " << pos << " \n";
			std::cout << color << "   *Wrong input, try again...\n";
			std::cin >> position;
			if (checkAbort(position))
			{
				std::cout << color << "    *The game has been aborted.....\n";
				system("pause");
				return;
			}
			pos = verifyInputPosition(position, board);
		}
		func.SwitchLights(pos, board, options);
		}
	}
	time = clock.getElapsedTime();
	std::cout << color << "   *Congratulations!! You finished the game in: " << time.asSeconds() << " seconds.\n";
	system("pause");
}

void Game::RunConsoleNormalGamePredefinedMap(std::string mapName,const Options& opt)
{
	Options options;
	options.SetOnColor(options.parseColorToString(opt.GetOnColor()));
	options.SetOffColor(options.parseColorToString(opt.GetOffColor()));
	Board board(options.GetHeight(), options.GetWidth());
	//init board//////////////////////////////////////////
	std::ifstream fin;
	std::string path = "Maps/";
	path += mapName;
	fin.open(path);
	int x;
	while (fin >> x)
	{
		Light l;
		if (x == 1)
			l.Switch();
		board.AddLight(l);
	}
	////////////////////////////////////////////////////
	Time time;
	Clock clock;
	BoardConsoleFunctions func;
	std::string position;
	rang::fgB color = rang::fgB::gray;
	bool startClock = true;
	//int i = 10;
	while (!GameSolved(board))
	{
		func.drawPlayScreen(board, options);
		std::cout << color << "   *Enter the position of the light which you would like to change: \n";
		std::cout << color << "    (Write 'exit' if you want to abandon the game): \n";
		std::cin >> position;
		if (checkAbort(position))
		{
			std::cout << color << "    *The game has been aborted.....\n";
			system("pause");
			return;
		}
		else
		{
			if (startClock)
			{
				startClock = false;
				clock.restart();
			}
			int pos = verifyInputPosition(position, board);
			while (pos == -1)
			{
				std::cout << "pos = " << pos << " \n";
				std::cout << color << "   *Wrong input, try again...\n";
				std::cin >> position;
				if (checkAbort(position))
				{
					std::cout << color << "    *The game has been aborted.....\n";
					system("pause");
					return;
				}
				pos = verifyInputPosition(position, board);
			}
			func.SwitchLights(pos, board, options);
		}
	}
	time = clock.getElapsedTime();
	std::cout << color << "   *Congratulations!! You finished the game in: " << time.asSeconds() << " seconds.\n";
	system("pause");
}

void Game::RunConsoleNormalGamePredefinedMapTimeAttack(std::string mapName, const Options& opt)
{
	Options options;
	options.SetOnColor(options.parseColorToString(opt.GetOnColor()));
	options.SetOffColor(options.parseColorToString(opt.GetOffColor()));
	Board board(options.GetHeight(), options.GetWidth());
	//init board//////////////////////////////////////////
	std::ifstream fin;
	std::string path = "Maps/";
	path += mapName;
	fin.open(path);
	int x;
	while (fin >> x)
	{
		Light l;
		if (x == 1)
			l.Switch();
		board.AddLight(l);
	}
	////////////////////////////////////////////////////
	Time time;
	Clock clock;
	Time timeUntilLoss = sf::seconds(180);
	BoardConsoleFunctions func;
	std::string position;
	rang::fgB color = rang::fgB::gray;
	bool startClock = true;
	//int i = 10;
	while (!GameSolved(board) && clock.getElapsedTime() <= timeUntilLoss)
	{
		func.drawPlayScreen(board, options);
		std::cout << color << "   *Enter the position of the light which you would like to change: \n";
		std::cout << color << "    (Write 'exit' if you want to abandon the game): \n";
		std::cin >> position;
		if (checkAbort(position))
		{
			std::cout << color << "    *The game has been aborted.....\n";
			system("pause");
			return;
		}
		else
		{
			if (startClock)
			{
				startClock = false;
				clock.restart();
			}
			int pos = verifyInputPosition(position, board);
			while (pos == -1)
			{
				std::cout << "pos = " << pos << " \n";
				std::cout << color << "   *Wrong input, try again...\n";
				std::cin >> position;
				if (checkAbort(position))
				{
					std::cout << color << "    *The game has been aborted.....\n";
					system("pause");
					return;
				}
				pos = verifyInputPosition(position, board);
			}
			func.SwitchLights(pos, board, options);
		}
	}
	time = clock.getElapsedTime();
	if (time < timeUntilLoss)
		std::cout << color << "   *Congratulations!! You finished the game in: " << time.asSeconds() << " seconds.\n";
	else
	{
		std::cout << color << "   *Sadly time's up. You lost! \n";
	}
	system("pause");
}

void Game::drawBoard(const std::vector<sf::Sprite>& lights, sf::RenderWindow& gameWindow)
{
	for (auto& each : lights)
	{
		gameWindow.draw(each);
	}
}

bool Game::GameSolved(Board &board) const
{
	for (Light light : board.GetLights())
	{
		if (light.GetState() == Light::State::On)
		{
			return false;
		}
	}
	return true;
}

bool Game::GameAborted(const Board& board) const
{
	return true;
}

void Game::initRandomBoard(Board& board, std::vector <sf::Sprite>& lightsVector, std::pair<sf::Sprite, sf::Sprite>OnOff,const Options& options) 
{
	//+5 makes sure that, random light will be turnt at least 5 times
	int randomNumberOfTimes = rand() % board.GetLights().size() + 5;
	int contorNumberOfTimes = 0;
	int randomPosition;
	while (contorNumberOfTimes < randomNumberOfTimes)
	{
		randomPosition = rand() % board.GetLights().size();
		board.SwitchLights(randomPosition, options, lightsVector, OnOff);
		//the contor incrementetion
		++contorNumberOfTimes;
	}
}

void Game::initPredefinedMap(Board& board, const Options& options)
{
	std::string fileName = options.GetPredefinedMap().getString();
	fileName.erase(0,5);
	std::ifstream f("./Maps/"+fileName+".txt");

	board.GetLights().clear();

	int index = 0;
	while (index<board.GetSize())
	{
		int aux;
		f >> aux;
		Light light;
		if (aux == 0)
		{
			board.AddLight(light);
		}
		if (aux == 1)
		{
			light.Switch();
			board.AddLight(light);
		}
		index++;
	}
}