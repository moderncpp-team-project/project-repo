#pragma once
#include <iostream>
#include <fstream>

class Logger
{
	enum class Level : uint16_t
	{
		LevelError, LevelWarning, LevelInfo
	};

public:

	Logger(const std::string& file);
	void SetLevel(const std::string& level);
	void Error(const std::string& message);
	void Warn(const std::string& message);
	void Info(const std::string& message);

private:
	Level m_LogLevel = Level::LevelInfo;
	std::ofstream m_LoggerOut;

private:
	const Level& parseStringToLevel(const std::string& toParse);
};

