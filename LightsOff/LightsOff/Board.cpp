#include "Board.h"
#include <iostream>
#include<fstream>

Board::Board(size_t width, size_t height)
	:m_width(width), m_height(height)
{
	m_size = m_width * m_height;
}

void Board::InitBoard()
{
	m_lights.clear();

	for (int i = 0; i < m_size; i++)
	{
		Light bulb;
		m_lights.emplace_back(bulb);
	}
}

void Board::AddLight(const Light& light)
{
	m_lights.emplace_back(light);
}

std::vector<Light>& Board::GetLights()
{
	return m_lights;

}

const size_t& Board::GetHeight() const
{
	return m_height;
}

const size_t& Board::GetWidth() const
{
	return m_width;
}

const size_t& Board::GetSize() const
{
	return m_size;
}

void Board::SetHeightAndWidth( const size_t& height,const size_t& width)
{
	this->m_width = width;
	this->m_height = height;
	this->m_size = height * width;
}

void Board::SwitchLights(const int& lightNumber,const Options& options, std::vector <sf::Sprite>& lightsVector, std::pair<sf::Sprite, sf::Sprite>OnOff)
{

	SwitchGUILight(lightNumber, lightsVector, OnOff);
	m_lights.at(lightNumber).Switch();
	if (options.GetUpDown())
	{
		SwitchLightUp(lightNumber,lightsVector,OnOff);
		SwitchLightDown(lightNumber, lightsVector, OnOff);
	}
	if (options.GetLeftRight())
	{
		SwitchLightLeft(lightNumber, lightsVector, OnOff);
		SwitchLightRight(lightNumber, lightsVector, OnOff);
	}
	if (options.GetMainDiagonal())
	{
		SwitchLightLeftUp(lightNumber, lightsVector, OnOff);
		SwitchLightRightDown(lightNumber, lightsVector, OnOff);
	}
	if (options.GetSecundaryDiagonal())
	{
		SwitchLightRightUp(lightNumber, lightsVector, OnOff);
		SwitchLightLeftDown(lightNumber, lightsVector, OnOff);
	}
}


void Board::SwitchGUILight(const int& lightNumber, std::vector <sf::Sprite>& lightsVector,std::pair<sf::Sprite, sf::Sprite>OnOff)
{
	if (lightsVector.size() > 0 && lightNumber < m_lights.size())
	{
		if (m_lights.at(lightNumber).GetState() == Light::State::On)
		{
			sf::Sprite switched = OnOff.second;
			switched.setPosition(lightsVector.at(lightNumber).getPosition());
			lightsVector.at(lightNumber) = switched;
		}
		else
		{
			sf::Sprite switched = OnOff.first;
			switched.setPosition(lightsVector.at(lightNumber).getPosition());
			lightsVector.at(lightNumber) = switched;
		}
	}
}


Light& Board::operator[](int position)
{
	if (position >= 0 && position < m_lights.size())
	{
		return m_lights[position];
	}
	else
	{
		throw("Index out of bounds");
	}
}


void Board::SwitchLightUp(const int& lightNumber, std::vector <sf::Sprite>& lightsVector, std::pair<sf::Sprite, sf::Sprite>OnOff)
{

	if (lightNumber >= m_width)
	{
		SwitchGUILight(lightNumber - m_width, lightsVector, OnOff);
		m_lights.at(lightNumber - m_width).Switch();
	}
}

void Board::SwitchLightDown(const int& lightNumber, std::vector <sf::Sprite>& lightsVector, std::pair<sf::Sprite, sf::Sprite>OnOff)
{
	if (lightNumber < m_size-m_width)
	{
		SwitchGUILight(lightNumber + m_width, lightsVector, OnOff);
		m_lights.at(lightNumber + m_width).Switch();
	}
}

void Board::SwitchLightLeft(const int& lightNumber, std::vector <sf::Sprite>& lightsVector,  std::pair<sf::Sprite, sf::Sprite>OnOff)
{
	if (lightNumber % m_width != 0)
	{
		SwitchGUILight(lightNumber - 1, lightsVector, OnOff);
		m_lights.at(lightNumber - 1).Switch();
	}
}

void Board::SwitchLightRight(const int& lightNumber, std::vector <sf::Sprite>& lightsVector,  std::pair<sf::Sprite, sf::Sprite>OnOff)
{
	if ((lightNumber + 1) % m_width != 0)
	{
		SwitchGUILight(lightNumber + 1, lightsVector, OnOff);
		m_lights.at(lightNumber + 1).Switch();
	}
}

void Board::SwitchLightLeftUp(const int& lightNumber, std::vector <sf::Sprite>& lightsVector,  std::pair<sf::Sprite, sf::Sprite>OnOff)
{
	if (lightNumber % m_width != 0 && lightNumber >= m_width)
	{
		SwitchGUILight(lightNumber - m_width - 1, lightsVector, OnOff);
		m_lights.at(lightNumber - m_width - 1).Switch();
	}
}

void Board::SwitchLightLeftDown(const int& lightNumber, std::vector <sf::Sprite>& lightsVector,  std::pair<sf::Sprite, sf::Sprite>OnOff)
{
	if (lightNumber % m_width != 0 && lightNumber < m_size - m_width)
	{
		SwitchGUILight(lightNumber + m_width - 1, lightsVector, OnOff);
		m_lights.at(lightNumber + m_width - 1).Switch();
	}
}

void Board::SwitchLightRightUp(const int& lightNumber, std::vector <sf::Sprite>& lightsVector,  std::pair<sf::Sprite, sf::Sprite>OnOff)
{
	if ((lightNumber + 1) % m_width != 0 && lightNumber >= m_width)
	{
		SwitchGUILight(lightNumber - m_width + 1, lightsVector, OnOff);
		m_lights.at(lightNumber - m_width + 1).Switch();
	}
}

void Board::SwitchLightRightDown(const int& lightNumber, std::vector <sf::Sprite>& lightsVector,  std::pair<sf::Sprite, sf::Sprite>OnOff)
{
	if ((lightNumber + 1) % m_width != 0 && lightNumber < m_size - m_width)
	{
		SwitchGUILight(lightNumber + m_width + 1, lightsVector, OnOff);
		m_lights.at(lightNumber + m_width + 1).Switch();
	}
}
