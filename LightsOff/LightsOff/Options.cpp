#include "Options.h"
#include"GameScreen.h"
#include"dirent.h"


Options::Options()
	:
	//Default Neighbourhood
	o_leftRight(true), o_upDown(true), o_mainDiagonal(false), o_secundaryDiagonal(false),
	//Default Board Size
	o_height(5), o_width(5), o_3x3(false), o_5x5(true), o_7x7(false), o_9x9(false),
	//Default Colors
	o_onColor(Colors::Yellow), o_offColor(Colors::Red),
	//Default Time
	o_timeAttack(false),
	//Default GameMode
	o_gameRandomMode(true),

	o_optionNumber(0),
	o_optionOnColor(0),
	o_optionOffColor(0),
	o_optionNeighbours(0),
	o_optionBoardSizes(1),
	o_optionPredefinedMapNumber(0)
{
	Text widthBoard;
	o_optionsList.push_back(widthBoard);
	Text heightBoard;
	o_optionsList.push_back(heightBoard);
	Text predefinedSizeBoard;
	o_optionsList.push_back(predefinedSizeBoard);
	Text lightOnColor;
	o_optionsList.push_back(lightOnColor);
	Text lightOffColor;
	o_optionsList.push_back(lightOffColor);
	Text neighbour;
	o_optionsList.push_back(neighbour);
	Text timeAttack;
	o_optionsList.push_back(timeAttack);
	Text gameRandomMode;
	o_optionsList.push_back(gameRandomMode);


}


#pragma region GetFunctions
const bool& Options::GetUpDown() const
{
	return o_upDown;
}

const bool& Options::GetLeftRight() const
{
	return o_leftRight;
}

const bool& Options::GetMainDiagonal() const
{
	return o_mainDiagonal;
}

const bool& Options::GetSecundaryDiagonal() const
{
	return o_secundaryDiagonal;
}

const Options::Colors& Options::GetOnColor() const
{
	return o_onColor;
}

const Options::Colors& Options::GetOffColor() const
{
	return o_offColor;
}

const uint16_t& Options::GetHeight() const
{
	return o_height;
}

const uint16_t& Options::GetWidth() const
{
	return o_width;
}

const bool& Options::GetGameRandomMode() const
{
	return o_gameRandomMode;
}

const bool& Options::GetTimeAttack() const
{
	return o_timeAttack;
}
const Text& Options::GetPredefinedMap() const
{
	return o_predefinedMaps[o_optionPredefinedMapNumber];
}
#pragma endregion


#pragma region GetStringFunctions

std::string Options::GetBoardPredefinedSize() const
{
	std::string rules;

	if (o_optionBoardSizes == 0)
	{
		rules.append(" 3x3 ");
	}
	if (o_optionBoardSizes == 1)
	{
		rules.append(" 5x5 ");
	}
	if (o_optionBoardSizes == 2)
	{
		rules.append(" 7x7 ");
	}
	if (o_optionBoardSizes == 3)
	{
		rules.append(" 9x9 ");
	}
	return rules;
}

std::string Options::GetNeighboursRules() const
{
	std::string rules;

	if (o_leftRight)
	{
		rules.append(" Left-Right ");
	}
	if (o_upDown)
	{
		rules.append(" Up-Down");
	}
	if (o_mainDiagonal)
	{
		rules.append(" Main-Diagonal ");
	}
	if (o_secundaryDiagonal)
	{
		rules.append(" Second-Diagonal");
	}
	return rules;
}

std::string Options::GetTimeAttackRule() const
{
	std::string rules;

	if (o_timeAttack)
	{
		rules.append(" On ");
	}
	else
	{
		rules.append(" Off ");
	}

	return rules;
}

std::string Options::GetGameRandomModeRule() const
{
	std::string rules;

	if (o_gameRandomMode)
	{
		rules.append(" On ");
	}
	else
	{
		rules.append(" Off ");
	}

	return rules;
}
#pragma endregion


#pragma region SetFunctions
void Options::SetUpDown(const bool& newState)
{
	o_upDown = newState;
}

void Options::SetLeftRight(const bool& newState)
{
	o_leftRight = newState;
}

void Options::SetMainDiagonal(const bool& newState)
{
	o_mainDiagonal = newState;
}

void Options::SetSecundaryDiagonal(const bool& newState)
{
	o_secundaryDiagonal = newState;
}

void Options::SetOnColor(const std::string& newOnColor)
{
	o_onColor = parseStringToColor(newOnColor);
}

void Options::SetOffColor(const std::string& newOffColor)
{
	o_offColor = parseStringToColor(newOffColor);
}

void Options::SetHeight(const uint16_t& newHeight)
{
	o_height = newHeight;
}

void Options::SetWidth(const uint16_t& newWidth)
{
	o_width = newWidth;
}
#pragma endregion


const Options::Colors& Options::parseStringToColor(const std::string& toParse)
{
	if (toParse.at(0) == 'B')
		return Colors::Blue;
	if (toParse[0] == 'C')
		return Colors::Cyan;
	if (toParse[0] == 'Y')
		return Colors::Yellow;
	if (toParse[0] == 'G')
		return Colors::Green;
	if (toParse[0] == 'M')
		return Colors::Magenta;
	return Colors::Red;
}

std::string Options::parseColorToString(const Options::Colors toParse)
{
	if (toParse == Colors::Blue)
		return "Blue";
	if (toParse == Colors::Cyan)
		return "Cyan";
	if (toParse == Colors::Green)
		return "Green";
	if (toParse == Colors::Magenta)
		return "Magenta";
	if (toParse == Colors::Red)
		return "Red";
	return "Yellow";
}

const MenuOption& Options::OptionsScreenDisplay(RenderWindow& gameWindow, GameScreen& gameScreen, int WINDOW_WIDTH, int WINDOW_HEIGHT, ResourceLoader& resourceLoader, Logger& logger,Event&event)
{

	SetPredefinedMaps();
	Text predefinedMap;
	o_optionsList.push_back(predefinedMap);

	gameWindow.draw(gameScreen.background);


	//afisam latimea boardului
	Text& widthBoard = o_optionsList[0];
	widthBoard.setString("Board width: " + std::to_string(o_width));
	widthBoard.setFont(resourceLoader.GetFont());
	float xWidthBoard = WINDOW_WIDTH / 2 - widthBoard.getLocalBounds().width / 2;
	float yWidthBoard = WINDOW_HEIGHT * 30 / 100;
	widthBoard.setPosition(xWidthBoard, yWidthBoard);
	widthBoard.setFillColor(Color(204, 204, 204));
	gameWindow.draw(widthBoard);

	//afisam inaltimea boardului
	Text& heightBoard = o_optionsList[1];
	heightBoard.setString("Board height: " + std::to_string(o_height));
	heightBoard.setFont(resourceLoader.GetFont());
	float xHeightBoard = WINDOW_WIDTH / 2 - heightBoard.getLocalBounds().width / 2;
	float yHeightBoard = WINDOW_HEIGHT * 35 / 100;
	heightBoard.setPosition(xHeightBoard, yHeightBoard);
	heightBoard.setFillColor(Color(204, 204, 204));
	gameWindow.draw(heightBoard);

	//afiseaza dimensiunule predefinite
	Text& predefinedSizeBoard = o_optionsList[2];
	predefinedSizeBoard.setString("Predefined size: " + GetBoardPredefinedSize());
	predefinedSizeBoard.setFont(resourceLoader.GetFont());
	float xPredefinedSizeBoard = WINDOW_WIDTH / 2 - predefinedSizeBoard.getLocalBounds().width / 2;
	float yPredefinedSizeBoard = WINDOW_HEIGHT * 40 / 100;
	predefinedSizeBoard.setPosition(xPredefinedSizeBoard, yPredefinedSizeBoard);
	predefinedSizeBoard.setFillColor(Color(204, 204, 204));
	gameWindow.draw(predefinedSizeBoard);

	//afiseaza culoarea becului aprins
	Text& lightOnColor = o_optionsList[3];
	lightOnColor.setString("Light ON color: ");
	lightOnColor.setFont(resourceLoader.GetFont());
	float xLightOnColor = WINDOW_WIDTH / 2 - lightOnColor.getLocalBounds().width / 2;
	float yLightOnColor = WINDOW_HEIGHT * 45 / 100;
	lightOnColor.setPosition(xLightOnColor, yLightOnColor);
	lightOnColor.setFillColor(Color(204, 204, 204));

	RectangleShape colorLightOn;
	colorLightOn.setPosition(Vector2f(xLightOnColor+lightOnColor.getLocalBounds().width,yLightOnColor));
	colorLightOn.setFillColor(parseEnumColorsToSFMLColor(o_onColor));
	colorLightOn.setSize(Vector2f(30, 30));
	gameWindow.draw(colorLightOn);
	gameWindow.draw(lightOnColor);

	//afiseaza culoarea becului stins
	Text& lightOffColor = o_optionsList[4];
	lightOffColor.setString("Light OFF color: ");
	lightOffColor.setFont(resourceLoader.GetFont());
	float xLightOffColor = WINDOW_WIDTH / 2 -lightOffColor.getLocalBounds().width / 2;
	float yLightOffColor = WINDOW_HEIGHT * 50 / 100;
	lightOffColor.setPosition(xLightOffColor, yLightOffColor);
	lightOffColor.setFillColor(Color(204, 204, 204));

	RectangleShape colorLightOff;
	colorLightOff.setPosition(Vector2f(xLightOffColor + lightOffColor.getLocalBounds().width, yLightOffColor));
	colorLightOff.setFillColor(parseEnumColorsToSFMLColor(o_offColor));
	colorLightOff.setSize(Vector2f(30, 30));
	gameWindow.draw(colorLightOff);
	gameWindow.draw(lightOffColor);

	//afiseaza vecinatatea care este setata
	Text& neighbour = o_optionsList[5];
	neighbour.setString("Neighbour rules: " + GetNeighboursRules());
	neighbour.setFont(resourceLoader.GetFont());
	float xNeighbour = WINDOW_WIDTH/2 - neighbour.getLocalBounds().width/2;
	float yNeighbour = WINDOW_HEIGHT * 55 / 100;
	neighbour.setPosition(xNeighbour, yNeighbour);
	neighbour.setFillColor(Color(204, 204, 204));
	gameWindow.draw(neighbour);

	//afiseaza optiunea de timeAttack
	Text& timeAttack = o_optionsList[6];
	timeAttack.setString("Time Attack: " + GetTimeAttackRule());
	timeAttack.setFont(resourceLoader.GetFont());
	float xTimeAttack = WINDOW_WIDTH / 2 - timeAttack.getLocalBounds().width / 2;
	float yTimeAttack = WINDOW_HEIGHT * 60 / 100;
	timeAttack.setPosition(xTimeAttack, yTimeAttack);
	timeAttack.setFillColor(Color(204, 204, 204));
	gameWindow.draw(timeAttack);

	//afiseaza modul de joc, randomizare mapa sau citire mapa
	Text& gameRandomMode = o_optionsList[7];
	gameRandomMode.setString(" Random map: " + GetGameRandomModeRule());
	gameRandomMode.setFont(resourceLoader.GetFont());
	float xgameRandomMode = WINDOW_WIDTH / 2 - gameRandomMode.getLocalBounds().width / 2;
	float ygameRandomMode = WINDOW_HEIGHT * 65 / 100;
	gameRandomMode.setPosition(xgameRandomMode, ygameRandomMode);
	gameRandomMode.setFillColor(Color(204, 204, 204));
	gameWindow.draw(gameRandomMode);


	//afisam lista de harti predefinite
	if (!o_gameRandomMode)
	{
		o_optionsList[8] = o_predefinedMaps[o_optionPredefinedMapNumber];
		Text& predefinedMap = o_optionsList[8];
		predefinedMap.setFont(resourceLoader.GetFont());
		float xPredefinedMapMode = WINDOW_WIDTH / 2 - predefinedMap.getLocalBounds().width / 2;
		float yPredefinedMapMode = WINDOW_HEIGHT * 70 / 100;
		predefinedMap.setPosition(xPredefinedMapMode, yPredefinedMapMode);
		predefinedMap.setFillColor(Color(204, 204, 204));
		gameWindow.draw(predefinedMap);
	}


	moveInOptionsMenu(event);

	//afiseaza butonul de back
	backButton.setTexture(resourceLoader.GetTexture(ResourceLoader::TextureType::UIButtonBack));
	backButton.setScale(.2, .2);
	float xBackButton = WINDOW_WIDTH * 5 / 100;
	float yBackButton = WINDOW_HEIGHT * 45 / 100;
	backButton.setPosition(xBackButton, yBackButton);
	gameWindow.draw(backButton);

	if (isBackButtonOrEscapePressed(gameWindow))
	{
		logger.Info("The user pressed the back button or hit ESC key, going back to main menu.");
		return MenuOption::Menu;
	}

	o_optionsList[o_optionNumber].setFillColor(Color::Cyan);
	gameWindow.draw(o_optionsList[o_optionNumber]);

	return MenuOption::Default;
}

bool Options::isBackButtonOrEscapePressed(RenderWindow& window) const
{
	if (Mouse::isButtonPressed(Mouse::Button::Left))
	{
		Vector2i mousePos = Mouse::getPosition(window);
		Vector2f mousePosAux(mousePos.x, mousePos.y);
		if (backButton.getGlobalBounds().contains(mousePosAux))
		{
			return true;
		}
	}
	
	if (Keyboard::isKeyPressed(Keyboard::Key::Escape))
	{
		return true;
	}
	return false;
}

void Options::LeftRightOption()
{
	o_leftRight = true;
	o_upDown = false;
	o_mainDiagonal = false;
	o_secundaryDiagonal = false;
}

void Options::UpDownOption()
{
	o_leftRight = false;
	o_upDown = true;
	o_mainDiagonal = false;
	o_secundaryDiagonal = false;
}

void Options::MainDiagonalOption()
{
	o_leftRight = false;
	o_upDown = false;
	o_mainDiagonal = true;
	o_secundaryDiagonal = false;
}

void Options::SecundaryDiagonalOption()
{
	o_leftRight = false;
	o_upDown = false;
	o_mainDiagonal = false;
	o_secundaryDiagonal = true;
}

void Options::AllNeighboursOption()
{
	o_leftRight = true;
	o_upDown = true;
	o_mainDiagonal = true;
	o_secundaryDiagonal = true;
}

void Options::UpDownLeftRightOption()
{
	o_leftRight = true;
	o_upDown = true;
	o_mainDiagonal = false;
	o_secundaryDiagonal = false;
}

void Options::switchForGUI()
{
	if (o_optionNeighbours == 6)
	{
		o_optionNeighbours = 0;
	}

	if (o_optionNeighbours == 0)
	{
		LeftRightOption();
	}
	if (o_optionNeighbours == 1)
	{
		UpDownOption();
	}
	if (o_optionNeighbours == 2)
	{
		MainDiagonalOption();
	}
	if (o_optionNeighbours == 3)
	{
		SecundaryDiagonalOption();
	}
	if (o_optionNeighbours == 4)
	{
		AllNeighboursOption();
	}
	if (o_optionNeighbours == 5)
	{
		UpDownLeftRightOption();
	}
	o_optionNeighbours++;
}

void Options::SetBoardPredefinedSizes()
{
	if (o_optionBoardSizes == 0)
	{
		o_3x3 == true;
		o_5x5 == false;
		o_7x7 == false;
		o_9x9 == false;
		o_width = 3;
		o_height = 3;
	}

	if (o_optionBoardSizes == 1)
	{
		o_3x3 == false;
		o_5x5 == true;
		o_7x7 == false;
		o_9x9 == false;
		o_width = 5;
		o_height = 5;
	}

	if (o_optionBoardSizes == 2)
	{
		o_3x3 == false;
		o_5x5 == false;
		o_7x7 == true;
		o_9x9 == false;
		o_width = 7;
		o_height = 7;
	}

	if (o_optionBoardSizes == 3)
	{
		o_3x3 == false;
		o_5x5 == false;
		o_7x7 == false;
		o_9x9 == true;
		o_width = 9;
		o_height = 9;
	}
}


std::vector<std::string> open(std::string path = ".") 
{

	DIR* dir;
	dirent* pdir;
	std::vector<std::string> files;

	dir = opendir(path.c_str());

	while (pdir = readdir(dir)) 
	{
		files.push_back(pdir->d_name);
	}

	files.erase(files.begin(),files.begin()+2);

	return files;
}


void Options::SetPredefinedMaps()
{
	std::vector<std::string>files;
	files = open("./Maps");
	std::ifstream file;

	for (auto i = 0; i < files.size(); i++)
	{
		Text aux;
		files[i].erase(files[i].find("."),4);
		aux.setString("Map: "+files[i]);
		o_predefinedMaps.emplace_back(aux);
	}

}


void Options::moveInOptionsMenu(Event& event)
{
	//din sagetile sus si jos se poate umbla in meniu
	if (event.type == Event::KeyPressed)
	{
		if (event.key.code == Keyboard::Down)
		{
			if (o_gameRandomMode)
			{
				if (o_optionNumber == 7)
				{
					o_optionNumber = 0;
				}
				else {
					o_optionNumber++;
				}
			}
			else {
				if (o_optionNumber == 8)
				{
					o_optionNumber = 0;
				}
				else {
					o_optionNumber++;
				}
			}
			
		}
		if (event.key.code == Keyboard::Up)
		{
			if (o_gameRandomMode)
			{
				if (o_optionNumber == 0)
				{
					o_optionNumber = 7;
				}
				else {
					o_optionNumber--;
				}
			}
			else {
				if (o_optionNumber == 0)
				{
					o_optionNumber = 8;
				}
				else {
					o_optionNumber--;
				}
			}
			
		}
	}

	//din sagetile dreapta-stanga se poate modifica setarea aferenta
	switch (o_optionNumber)
	{
	case 0:
		//modificare pentru width
		if (event.type == sf::Event::KeyPressed)
		{
			if (event.key.code == sf::Keyboard::Right)
			{
				if (o_width == 9)
				{
					o_width = 3;
				}
				else 
				{
					o_width++;
				}
				o_gameRandomMode = true;
			}
			if (event.key.code == sf::Keyboard::Left)
			{
				if (o_width == 3)
				{
					o_width = 9;
				}
				else 
				{
					o_width--;
				}
				o_gameRandomMode = true;
			}			
		}
		break;

		//modificare height
	case 1:
		if (event.type == sf::Event::KeyPressed)
		{
			if (event.key.code == sf::Keyboard::Right)
			{
				if (o_height == 9)
				{
					o_height = 3;
				}
				else 
				{
					o_height++;
				}
				o_gameRandomMode = true;
			}
			if (event.key.code == sf::Keyboard::Left)
			{
				if (o_height == 3)
				{
					o_height = 9;
				}
				else 
				{
					o_height--;
				}
				o_gameRandomMode = true;
			}			
		}
		break;

		//modificare dimensiune predefinita
	case 2:
		if (event.type == sf::Event::KeyPressed)
		{
			if (event.key.code == sf::Keyboard::Right)
			{
				if (o_optionBoardSizes == 3)
				{
					o_optionBoardSizes = 0;
				}
				else
				{
					o_optionBoardSizes++;
				}

				SetBoardPredefinedSizes();
				o_gameRandomMode = true;
			}

			if (event.key.code == sf::Keyboard::Left)
			{
				if (o_optionBoardSizes == 0)
				{
					o_optionBoardSizes = 3;
				}
				else
				{
					o_optionBoardSizes--;
				}

				SetBoardPredefinedSizes();
				o_gameRandomMode = true;
			}
		}
		break;

		//modificare culoare on
	case 3:
		if (event.type == sf::Event::KeyPressed)
		{
			if (event.key.code == sf::Keyboard::Right)
			{
				if (o_optionOnColor == 6)
				{
					o_optionOnColor = 0;
				}

				if (o_optionOnColor == 0)
				{
					o_onColor = Colors::Blue;
				}
				if (o_optionOnColor == 1)
				{
					o_onColor = Colors::Magenta;
				}
				if (o_optionOnColor == 2)
				{
					o_onColor = Colors::Cyan;
				}
				if (o_optionOnColor == 3)
				{
					o_onColor = Colors::Green;
				}
				if (o_optionOnColor == 4)
				{
					o_onColor = Colors::Red;
				}
				if (o_optionOnColor == 5)
				{
					o_onColor = Colors::Yellow;
				}
				o_optionOnColor++;
			}

			if (event.key.code == sf::Keyboard::Left)
			{
				if (o_optionOnColor == 0)
				{
					o_optionOnColor = 6;
				}
				o_optionOnColor--;
				o_optionOnColor--;

				if (o_optionOnColor == 0)
				{
					o_onColor = Colors::Blue;
				}
				if (o_optionOnColor == 1)
				{
					o_onColor = Colors::Magenta;
				}
				if (o_optionOnColor == 2)
				{
					o_onColor = Colors::Cyan;
				}
				if (o_optionOnColor == 3)
				{
					o_onColor = Colors::Green;
				}
				if (o_optionOnColor == 4)
				{
					o_onColor = Colors::Red;
				}
				if (o_optionOnColor == 5)
				{
					o_onColor = Colors::Yellow;
				}
				o_optionOnColor++;
			}
		}
		break;

	case 4:
		if (event.type == sf::Event::KeyPressed)
		{
			if (event.key.code == sf::Keyboard::Right)
			{
				if (o_optionOffColor == 6)
				{
					o_optionOffColor = 0;
				}

				if (o_optionOffColor == 0)
				{
					o_offColor = Colors::Blue;
				}
				if (o_optionOffColor == 1)
				{
					o_offColor = Colors::Magenta;
				}
				if (o_optionOffColor == 2)
				{
					o_offColor = Colors::Cyan;
				}
				if (o_optionOffColor == 3)
				{
					o_offColor = Colors::Green;
				}
				if (o_optionOffColor == 4)
				{
					o_offColor = Colors::Red;
				}
				if (o_optionOffColor == 5)
				{
					o_offColor = Colors::Yellow;
				}
				o_optionOffColor++;
			}

			if (event.key.code == sf::Keyboard::Left)
			{
				if (o_optionOffColor == 0)
				{
					o_optionOffColor = 6;
				}
				o_optionOffColor--;
				o_optionOffColor--;

				if (o_optionOffColor == 0)
				{
					o_offColor = Colors::Blue;
				}
				if (o_optionOffColor == 1)
				{
					o_offColor = Colors::Magenta;
				}
				if (o_optionOffColor == 2)
				{
					o_offColor = Colors::Cyan;
				}
				if (o_optionOffColor == 3)
				{
					o_offColor = Colors::Green;
				}
				if (o_optionOffColor == 4)
				{
					o_offColor = Colors::Red;
				}
				if (o_optionOffColor == 5)
				{
					o_offColor = Colors::Yellow;
				}
				o_optionOffColor++;
			}
		}
		break;

		//modificare vecini
	case 5:
		if (event.type == sf::Event::KeyPressed)
		{
			if (event.key.code == sf::Keyboard::Right)
			{
				if (o_optionNeighbours == 6)
				{
					o_optionNeighbours = 0;
				}

				if (o_optionNeighbours == 0)
				{
					LeftRightOption();
				}
				if (o_optionNeighbours == 1)
				{
					UpDownOption();
				}
				if (o_optionNeighbours == 2)
				{
					MainDiagonalOption();
				}
				if (o_optionNeighbours == 3)
				{
					SecundaryDiagonalOption();
				}
				if (o_optionNeighbours == 4)
				{
					AllNeighboursOption();
				}
				if (o_optionNeighbours == 5)
				{
					UpDownLeftRightOption();
				}
				o_optionNeighbours++;				
			}

			if (event.key.code == sf::Keyboard::Left)
			{
				if (o_optionNeighbours == 0)
				{
					o_optionNeighbours = 6;
				}
				o_optionNeighbours--;
				o_optionNeighbours--;

				if (o_optionNeighbours == 0)
				{
					LeftRightOption();
				}
				if (o_optionNeighbours == 1)
				{
					UpDownOption();
				}
				if (o_optionNeighbours == 2)
				{
					MainDiagonalOption();
				}
				if (o_optionNeighbours == 3)
				{
					SecundaryDiagonalOption();
				}
				if (o_optionNeighbours == 4)
				{
					AllNeighboursOption();
				}
				if (o_optionNeighbours == 5)
				{
					UpDownLeftRightOption();
				}
				o_optionNeighbours++;
			}

			if (o_optionNeighbours != 5)
			{
				o_gameRandomMode = true;
			}
		}
		break;

		//modificare timeAttack
	case 6:
		if (event.type == sf::Event::KeyPressed)
		{
			if (event.key.code == sf::Keyboard::Right)
			{
				if (o_timeAttack == false)
				{
					o_timeAttack = true;
				}
				else
				{
					o_timeAttack = false;
				}
			}

			if (event.key.code == sf::Keyboard::Left)
			{
				if (o_timeAttack == false)
				{
					o_timeAttack = true;
				}
				else
				{
					o_timeAttack = false;
				}
			}
		}
		break;
	case 7:
		//modificare mod de joc, random sau citire fisier
		if (event.type == sf::Event::KeyPressed)
		{
			if (event.key.code == sf::Keyboard::Right)
			{
				if (o_gameRandomMode == false)
				{
					o_gameRandomMode = true;
				}
				else
				{
					o_gameRandomMode = false;
				}
				o_optionBoardSizes = 1;
				SetBoardPredefinedSizes();
			}

			if (event.key.code == sf::Keyboard::Left)
			{
				if (o_gameRandomMode == false)
				{
					o_gameRandomMode = true;
				}
				else
				{
					o_gameRandomMode = false;
				}
				o_optionBoardSizes = 1;
				SetBoardPredefinedSizes();
			}

			if (o_gameRandomMode == false)
			{
				o_optionNeighbours = 5;
				UpDownLeftRightOption();
			}
		}
		break;
	case 8:
		//schimbare mapa predefinita
		if (event.type == sf::Event::KeyPressed)
		{
			if (event.key.code == sf::Keyboard::Right)
			{
				if (o_optionPredefinedMapNumber == o_predefinedMaps.size()-1)
				{
					o_optionPredefinedMapNumber = 0;
				}
				else 
				{
					o_optionPredefinedMapNumber++;
				}
			}

			if (event.key.code == sf::Keyboard::Left)
			{
				if (o_optionPredefinedMapNumber == 0)
				{
					o_optionPredefinedMapNumber = o_predefinedMaps.size()-1;
				}
				else 
				{
					o_optionPredefinedMapNumber--;
				}
			}			
			o_optionBoardSizes = 1;
			SetBoardPredefinedSizes();
		}
	default:
		break;
	}
	

	//se modifica culoarea optiunii pentru optiunea curenta
	for (uint16_t i = 0; i < o_optionsList.size(); i++)
	{
		if (i == o_optionNumber)
		{
			o_optionsList[i].setFillColor(sf::Color(204, 204, 204));
		}
		else 
		{
			o_optionsList[i].setFillColor(sf::Color::Cyan);
		}
	}
}

sf::Color Options::parseEnumColorsToSFMLColor(Colors c)
{
	switch (c)
	{
	case Colors::Blue:
		return sf::Color::Blue;
		break;
	case Colors::Cyan:
		return sf::Color::Cyan;
		break;
	case Colors::Green:
		return sf::Color::Green;
		break;
	case Colors::Magenta:
		return sf::Color::Magenta;
		break;
	case Colors::Red:
		return sf::Color::Red;
		break;
	}
	return sf::Color::Yellow;
}