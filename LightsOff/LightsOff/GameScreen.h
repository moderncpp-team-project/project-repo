#pragma once
#include<SFML/Graphics.hpp>
#include<iostream>
#include "ResourceLoader.h"
#include"Board.h"
#include"Options.h"
#include "Logger.h"

using namespace sf;

class GameScreen
{
public:
	Sprite background, lightOn, lightOff, backButton;

	void GameScreenSetTextures(const uint16_t& WINDOW_WIDTH, const uint16_t& WINDOW_HEIGHT, ResourceLoader& resourceLoader, Options& options);
	
	void GameScreenDisplay(RenderWindow& gameWindow, GameScreen& gameScreen, const uint16_t& WINDOW_WIDTH, const uint16_t& WINDOW_HEIGHT, Board& board, Logger& logger, const Options& options);
	
	int PositionOfLightPressed(const std::vector <sf::Sprite>& lightsVector, RenderWindow& gameWindow);
	
	bool isBackButtonOrEscapePressed(RenderWindow& window);
};