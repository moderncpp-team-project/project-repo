#include "ResourceLoader.h"
#include <iostream>
#include <thread>

bool ResourceLoader::Init()
{
	bool perfectLoad = true;

	std::thread fontThread([this, &perfectLoad]() 
	{
		if (m_textFont.loadFromFile("Font/Beaufort W01 Regular.ttf") == false)
		{
			std::cout << ("Error: couldn't load font.\n");
			perfectLoad = false;
		}
	});
		
	std::thread musicThreadLight([this, &perfectLoad]() 
	{
		if (m_musicLightSwitch.openFromFile("Musics/SwitchClick.wav") == false)
		{
			printf("Error: couldn't load switch music.\n");
			perfectLoad = false;
		}
	});

	std::thread backgroundThread([this, &perfectLoad]() 
	{
		if (m_background.loadFromFile("Textures/BackgroundStar4k.jpg") == false)
		{
			printf("Error: couldn't load background.\n");
			perfectLoad = false;
		}
	});

	std::thread lightOffThread([this, &perfectLoad]() 
	{
		if (m_lightOff.loadFromFile("Textures/LightOff.png") == false)
		{
			printf("Error: couldn't load lightOff texture.\n");
			perfectLoad = false;
		}
	});

	std::thread lightOnThread([this, &perfectLoad]() 
	{
		if (m_lightOn.loadFromFile("Textures/LightOn.png") == false)
		{
			printf("Error: couldn't load lightOn texture.\n");
			perfectLoad = false;
		}
	});

	std::thread playButtonThread([this, &perfectLoad]() 
	{
		if (m_UIButtonPlay.loadFromFile("Textures/Play.png") == false)
		{
			printf("Error: couldn't load play button.\n");
			perfectLoad = false;
		}
	});

	std::thread exitButtonThread([this, &perfectLoad]() 
	{
		if (m_UIButtonExit.loadFromFile("Textures/Exit.png") == false)
		{
			printf("Error: couldn't load exit button.\n");
			perfectLoad = false;
		}
	});	

	std::thread optionsButtonThread([this, &perfectLoad]() 
	{
		if (m_UIButtonOptions.loadFromFile("Textures/Option.png") == false)
		{
			printf("Error: couldn't load options button.\n");
			perfectLoad = false;
		}
	});
	
	std::thread backButtonThread([this, &perfectLoad]() 
	{
		if (m_UIButtonBack.loadFromFile("Textures/Back.png") == false)
		{
			printf("Error: couldn't load back button.\n");
			perfectLoad = false;
		}
	});

	fontThread.join();	
	musicThreadLight.join();
	backgroundThread.join();
	lightOffThread.join();
	lightOnThread.join();
	playButtonThread.join();
	optionsButtonThread.join();
	exitButtonThread.join();
	backButtonThread.join();

	return perfectLoad;
}

const sf::Texture& ResourceLoader::GetTexture(const TextureType& type) const
{
	switch (type)
	{
	case ResourceLoader::TextureType::Background:
	{
		return m_background;
	}
	case ResourceLoader::TextureType::LightOff:
	{
		return m_lightOff;
	}
	case ResourceLoader::TextureType::LightOn:
	{
		return m_lightOn;
	}
	case ResourceLoader::TextureType::UIButtonPlay:
	{
		return m_UIButtonPlay;
	}
	case ResourceLoader::TextureType::UIButtonExit:
	{
		return m_UIButtonExit;
	}
	case ResourceLoader::TextureType::UIButtonOptions:
	{
		return m_UIButtonOptions;
	}	
	case ResourceLoader::TextureType::UIButtonBack:
	{
		return m_UIButtonBack;
	}
	}
}

const sf::Font& ResourceLoader::GetFont() const
{
	return m_textFont;
}

sf::Music& ResourceLoader::GetMusicLightSwitch()
{
	return m_musicLightSwitch;
}