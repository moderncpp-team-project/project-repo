#include "Logger.h"
#include <chrono> 
#include <ctime> 
#pragma warning(disable : 4996)

Logger::Logger(const std::string& file)
{
	if (!m_LoggerOut.is_open())
	{
		m_LoggerOut.open(file);
	}
}

void Logger::SetLevel(const std::string& level)
{
	m_LogLevel = parseStringToLevel(level);
}

void Logger::Error(const std::string& message)
{
	if (m_LogLevel >= Level::LevelError)
	{
		auto timenow = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());

		m_LoggerOut << "[ERROR] :: " << ctime(&timenow) << message << std::endl << std::endl;
	}
}

void Logger::Warn(const std::string& message)
{
	if (m_LogLevel >= Level::LevelWarning)
	{
		auto timenow = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());

		m_LoggerOut << "[WARNING] :: " << ctime(&timenow) << message << std::endl << std::endl;
	}
}

void Logger::Info(const std::string& message)
{
	if (m_LogLevel >= Level::LevelInfo)
	{
		auto timenow = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());

		m_LoggerOut << "[INFO] :: " << ctime(&timenow) << message << std::endl << std::endl;
	}
}

const Logger::Level& Logger::parseStringToLevel(const std::string& toParse)
{
	if (toParse == "Error") return Level::LevelError;
	if (toParse == "Warning") return Level::LevelWarning;
	if (toParse == "Info") return Level::LevelInfo;
	else
	{
		std::cout << "Incorrect parseStringToLevel param input! Setting to LevelInfo" << std::endl;
		return Level::LevelInfo;
	}
}
