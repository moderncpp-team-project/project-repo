#pragma once
#include "Light.h"
#include <string>
#include <vector>
#include<SFML/Graphics.hpp>
#include "Options.h"
class Board
{
public:
	Board(size_t width, size_t height);
	
	void InitBoard();
	
	void AddLight(const Light& light);
	
	std::vector<Light>& GetLights();
	
	const size_t& GetHeight()const;
	
	const size_t& GetWidth()const;
	
	const size_t& GetSize()const;
	
	void SetHeightAndWidth(const size_t& height,const size_t& width);

	void SwitchLights(const int& lightNumber,const Options& options, std::vector <sf::Sprite>& lightsVector, std::pair<sf::Sprite, sf::Sprite>OnOff);
	
	void SwitchGUILight(const int& lightNumber, std::vector <sf::Sprite>& lightsVector, std::pair<sf::Sprite, sf::Sprite>OnOff);
	
	Light& operator [](int position);

protected:
	void SwitchLightUp(const int& lightNumber, std::vector <sf::Sprite>& lightsVector,std::pair<sf::Sprite, sf::Sprite>OnOff);
	void SwitchLightDown(const int& lightNumber, std::vector <sf::Sprite>& lightsVector,std::pair<sf::Sprite, sf::Sprite>OnOff);
	void SwitchLightLeft(const int& lightNumber, std::vector <sf::Sprite>& lightsVector,std::pair<sf::Sprite, sf::Sprite>OnOff);
	void SwitchLightRight(const int& lightNumber, std::vector <sf::Sprite>& lightsVector,std::pair<sf::Sprite, sf::Sprite>OnOff);
	
	
	void SwitchLightLeftUp(const int& lightNumber, std::vector <sf::Sprite>& lightsVector, std::pair<sf::Sprite, sf::Sprite>OnOff);
	void SwitchLightLeftDown(const int& lightNumber, std::vector <sf::Sprite>& lightsVector, std::pair<sf::Sprite, sf::Sprite>OnOff);

	void SwitchLightRightUp(const int& lightNumber, std::vector <sf::Sprite>& lightsVector,std::pair<sf::Sprite, sf::Sprite>OnOff);
	void SwitchLightRightDown(const int& lightNumber, std::vector <sf::Sprite>& lightsVector,std::pair<sf::Sprite, sf::Sprite>OnOff);


private:
	size_t m_width;
	size_t m_height;
	size_t m_size;
	std::vector<Light> m_lights;
};