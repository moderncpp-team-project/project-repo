#pragma once
#include <SFML/Graphics.hpp>
#include "Light.h"
#include "Board.h"
#include <ctime>
#include "GameScreen.h"
#include "Logger.h"

class Game
{
public:

	Game();
	
	void RunConsoleNormalGamePredefinedMapTimeAttack(std::string mapName, const Options& opt);

	void Run(Board& board,sf::RenderWindow& gameWindow,GameScreen& object, const uint16_t& WINDOW_WIDTH, const uint16_t& WINDOW_HEIGHT, Logger& logger,const Options& options);
	
	bool GameSolved(Board& board) const;
	
	bool GameAborted(const Board& board) const;
	
	void initRandomBoard(Board& board, std::vector <sf::Sprite>& lightsVector, std::pair<sf::Sprite, sf::Sprite>OnOff,const Options& options);
	
	void initPredefinedMap(Board& board,const Options&options);

	void drawBoard(const std::vector<sf::Sprite>& lights, sf::RenderWindow& gameWindow);
	
	void RunConsoleNormalGame(const Options& options);
	
	void initRandomBoardConsole(Board& board, const Options& options);
	
	void RunConsoleTimeAttackGame(const Options& options);
	
	void RunConsoleNormalGamePredefinedMap(std::string mapName, const Options& opt);
};


