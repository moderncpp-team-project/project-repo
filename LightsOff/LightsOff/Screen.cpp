#include "Screen.h"
#include "Game.h"
#include "GameScreen.h"
#include "MenuOption.h"

void Screen::MainScreenSetTextures(const uint16_t& WINDOW_WIDTH, const uint16_t& WINDOW_HEIGHT, ResourceLoader& resourceLoader)
{
	background.setTexture(resourceLoader.GetTexture(ResourceLoader::TextureType::Background));
	background.setScale(1, 1);

	playButton.setTexture(resourceLoader.GetTexture(ResourceLoader::TextureType::UIButtonPlay));
	playButton.setColor(sf::Color::White);
	playButton.setScale(.1, .1);
	playButton.setPosition(WINDOW_WIDTH * 5 / 100, WINDOW_HEIGHT * 10 / 100);

	exitButton.setTexture(resourceLoader.GetTexture(ResourceLoader::TextureType::UIButtonExit));
	exitButton.setColor(sf::Color::White);
	exitButton.setScale(.15, .15);
	exitButton.setPosition(WINDOW_WIDTH * 5 / 100, WINDOW_HEIGHT * 70 / 100);

	optionsButton.setTexture(resourceLoader.GetTexture(ResourceLoader::TextureType::UIButtonOptions));
	optionsButton.setColor(sf::Color::White);
	optionsButton.setScale(.18, .18);
	optionsButton.setPosition(WINDOW_WIDTH * 5 / 100, WINDOW_HEIGHT * 22 / 100);
}

void Screen::MainScreenSetTexts(const uint16_t& WINDOW_WIDTH, const uint16_t& WINDOW_HEIGHT, ResourceLoader& resourceLoader)
{
	welcomeText.setFont(resourceLoader.GetFont());
	welcomeText.setString("Welcome to LightsOff!\n         Have fun!");
	welcomeText.setScale(2, 2);
	welcomeText.setFillColor(Color(204, 204, 204));
	welcomeText.setCharacterSize(40);
	welcomeText.setLetterSpacing(2);
	welcomeText.setPosition(WINDOW_WIDTH * 30 / 100, WINDOW_HEIGHT * 35 / 100);

	playText.setFont(resourceLoader.GetFont());
	playText.setString("Play");
	playText.setScale(1.5, 1.5);
	playText.setFillColor(Color(204, 204, 204));
	playText.setPosition(playButton.getPosition().x + 100, playButton.getPosition().y + 20);

	optionsText.setFont(resourceLoader.GetFont());
	optionsText.setString("Options");
	optionsText.setScale(1.5, 1.5);
	optionsText.setFillColor(Color(204, 204, 204));
	optionsText.setPosition(optionsButton.getPosition().x + 100, optionsButton.getPosition().y + 20);

	exitText.setFont(resourceLoader.GetFont());
	exitText.setString("Exit");
	exitText.setScale(1.5, 1.5);
	exitText.setFillColor(Color(204, 204, 204));
	exitText.setPosition(exitButton.getPosition().x + 85, exitButton.getPosition().y + 10);
}

const MenuOption& Screen::MainScreenDisplay(RenderWindow& gameWindow, Screen& object, Logger& logger)
{
	
		gameWindow.draw(object.background);
		gameWindow.draw(object.welcomeText);
		gameWindow.draw(object.playButton);
		gameWindow.draw(object.playText);

		//daca butonul play este apasat fa ceva
		if (PlayButtonIsPressed(gameWindow))
		{
			logger.Info("The user has pressed the play button.");
			return MenuOption::Play;
		}

		gameWindow.draw(object.exitButton);
		gameWindow.draw(object.exitText);

		//daca butonul exit este apasat fa ceva
		if (ExitButtonIsPressed(gameWindow))
		{
			logger.Info("The user has pressed the exit button.");
			return MenuOption::Exit;
		}

		gameWindow.draw(object.optionsButton);
		gameWindow.draw(object.optionsText);

		//daca butonul options este apasat fa ceva
		if (OptionsButtonIsPressed(gameWindow))
		{
			logger.Info("The user has pressed the options button.");
			return MenuOption::Options;
		}
		return MenuOption::Default;

}

bool Screen::PlayButtonIsPressed(RenderWindow& window) const
{

		if (Mouse::isButtonPressed(Mouse::Button::Left))
		{
			Vector2i mousePos = Mouse::getPosition(window);
			Vector2f mousePosAux(mousePos.x, mousePos.y);
			if (playButton.getGlobalBounds().contains(mousePosAux))
			{
				return true;
			}
			if (playText.getGlobalBounds().contains(mousePosAux))
			{
				return true;
			}
		}
		return false;
}

bool Screen::OptionsButtonIsPressed(RenderWindow& window) const
{
	if (Mouse::isButtonPressed(Mouse::Button::Left))
	{
		Vector2i mousePos = Mouse::getPosition(window);
		Vector2f mousePosAux(mousePos.x, mousePos.y);
		if (optionsButton.getGlobalBounds().contains(mousePosAux))
		{
			return true;
		}
		if (optionsText.getGlobalBounds().contains(mousePosAux))
		{
			return true;
		}
	}
	return false;
}

bool Screen::ExitButtonIsPressed(RenderWindow& window) const
{
	if (Mouse::isButtonPressed(Mouse::Button::Left))
	{
		Vector2i mousePos = Mouse::getPosition(window);
		Vector2f mousePosAux(mousePos.x, mousePos.y);
		if (exitButton.getGlobalBounds().contains(mousePosAux))
		{
			return true;
		}
		if (exitText.getGlobalBounds().contains(mousePosAux))
		{
			return true;
		}
	}
	return false;
}

