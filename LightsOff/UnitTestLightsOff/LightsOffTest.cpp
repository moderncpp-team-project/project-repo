#pragma once
#include "pch.h"
#include "CppUnitTest.h"

#include "../LightsOff/Board.cpp"
#include "../LightsOff/Light.cpp"
#include "../LightsOff/Game.cpp"
#include "../LightsOff/Options.cpp"
#include "../LightsOff/ResourceLoader.cpp"
#include "../LightsOff/Logger.cpp"
#include "../LightsOff/GameScreen.cpp"
#include "../LightsOff/Screen.cpp"
#include "../LightsOff/BoardConsoleFunctions.cpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTestLightsOff
{
	TEST_CLASS(BoardTest)
	{
	public:

		TEST_METHOD(DefaultLightConstructor)
		{
			Light object;
			Assert::IsTrue(object.GetState() == Light::State::Off);
		}

		TEST_METHOD(LightSetState)
		{
			Light object;
			object.SetState(Light::State::On);
			Assert::IsTrue(object.GetState() == Light::State::On);
		}

		TEST_METHOD(LightSwitch)
		{
			Light object;
			object.Switch();
			Assert::IsTrue(object.GetState() == Light::State::On);
		}

		TEST_METHOD(DefaultBoardConstructor)
		{
			Board board(5, 5);
			Assert::IsTrue(board.GetWidth() == 5);
			Assert::IsTrue(board.GetHeight() == 5);
		}

		TEST_METHOD(InitBoardTest)
		{
			Board board(5, 5);
			board.InitBoard();

			Assert::IsTrue(board.GetLights().size() == board.GetSize());
		}


		TEST_METHOD(BoardSetHeightAndWidthTest)
		{
			Board board(4, 3);
			board.SetHeightAndWidth(6, 5);

			Assert::IsTrue(board.GetWidth() == 5);
			Assert::IsTrue(board.GetHeight() == 6);
		}

		TEST_METHOD(BoardAddLightSetHeightAndWidthTest)
		{
			Board board(7, 7);
			board.SetHeightAndWidth(9, 4);
			board.InitBoard();

			Light newLight;
			board.AddLight(newLight);

			Assert::IsTrue(board.GetLights().size() == 37);
		}

		TEST_METHOD(DefaultLightColor)
		{
			Options options;

			Assert::IsTrue(options.GetOnColor() == Options::Colors::Yellow);
			Assert::IsTrue(options.GetOffColor() == Options::Colors::Red);
		}

		TEST_METHOD(LightColorChange)
		{
			Options options;

			options.SetOnColor("Blue");
			options.SetOffColor("Cyan");
			Assert::IsTrue(options.GetOnColor() == Options::Colors::Blue);
			Assert::IsTrue(options.GetOffColor() == Options::Colors::Cyan);
		}

		TEST_METHOD(DefaultTimeAttackRandomGameMode)
		{
			Options options;

			Assert::IsTrue(options.GetTimeAttack() == false);
			Assert::IsTrue(options.GetGameRandomMode() == true);
		}

		TEST_METHOD(OptionsUpDownLeftRightTest)
		{
			Options options;
			options.SetUpDown(false);
			options.SetLeftRight(false);

			Assert::IsFalse(options.GetLeftRight());
			Assert::IsFalse(options.GetUpDown());
		}

		TEST_METHOD(GameNotSolvedTest)
		{
			Board board(7, 7);
			Game game;

			board.InitBoard();
			board.GetLights()[0].Switch();

			Assert::IsTrue(game.GameSolved(board) == false);
		}

		TEST_METHOD(GameSolvedTest)
		{
			Board board(5, 5);
			Game game;

			board.InitBoard();
			for (int index = 0; index < board.GetSize(); index++)
			{
				board.GetLights()[index].Switch();
				board.GetLights()[index].Switch();
			}

			Assert::IsTrue(game.GameSolved(board) == true);
		}

		TEST_METHOD(DefaultOptionsConstructor)
		{
			Options options;
			Assert::IsTrue(options.GetHeight() == 5);
			Assert::IsTrue(options.GetWidth() == 5);
			Assert::IsTrue(options.GetOffColor() == Options::Colors::Red);
			Assert::IsTrue(options.GetOnColor() == Options::Colors::Yellow);

		}

		TEST_METHOD(GameVerifyInputPosition)
		{
			Board board(5, 5);

			std::string position = { "4" };

			Assert::IsTrue(verifyInputPosition(position, board) == 4);
		}

		TEST_METHOD(GameAborted)
		{
			Board board(5, 5);
			Game game;
			Assert::IsTrue(game.GameAborted(board));
		}

		TEST_METHOD(PlayButtonIsPressed)
		{
			RenderWindow uindou;
			Screen screen;

			Assert::IsFalse(screen.PlayButtonIsPressed(uindou));
		}

		TEST_METHOD(OptionsButtonIsPressed)
		{
			RenderWindow uindou;
			Screen screen;

			Assert::IsFalse(screen.OptionsButtonIsPressed(uindou));
		}

		TEST_METHOD(ExitButtonIsPressed)
		{
			RenderWindow uindou;
			Screen screen;

			Assert::IsFalse(screen.ExitButtonIsPressed(uindou));
		}

		TEST_METHOD(BackButtonOrEscapeButtonPressed)
		{
			RenderWindow uindou;
			Options option;

			Assert::IsFalse(option.isBackButtonOrEscapePressed(uindou));
		}

		TEST_METHOD(BackButtonOrEscapeButtonPressedGameScreen)
		{
			RenderWindow uindou;
			GameScreen gameScreen;

			Assert::IsTrue(gameScreen.isBackButtonOrEscapePressed(uindou) == false);
		}

		TEST_METHOD(AddLight)
		{
			Board board(5, 5);
			Light light;

			board.AddLight(light);
			Assert::IsTrue(board.GetLights().size() != NULL);
		}

		TEST_METHOD(BoardOperatorOverload)
		{
			Board board(5, 5);
			board.InitBoard();

			board[5].Switch();

			Assert::IsTrue(board[5].GetState() == Light::State::On);
		}
	};
}