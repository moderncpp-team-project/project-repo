#include "pch.h"
#include "CppUnitTest.h"

#include "../LightsOff/Board.cpp"
#include "../LightsOff/Options.cpp"
#include "../LightsOff/ResourceLoader.cpp"
#include "../LightsOff/Logger.cpp"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTestLightsOff
{
	TEST_CLASS(BoardTest)
	{
	public:

		TEST_METHOD(DefaultBoardConstructor)
		{
			Board board(5, 5);

			Assert::IsTrue(board.GetWidth() == 5);
			Assert::IsTrue(board.GetHeight() == 5);
		}

		TEST_METHOD(InitBoardTest)
		{
			Board board(5, 5);
			board.InitBoard();

			Assert::IsTrue(board.GetLights().size() == board.GetSize());
		}

		TEST_METHOD(SetHeightAndWidthTest)
		{
			Board board(4, 3);
			board.SetHeightAndWidth(6, 5);

			Assert::IsTrue(board.GetWidth() == 6);
			Assert::IsTrue(board.GetHeight() == 5);
		}

	};
}