#include "pch.h"
#include "CppUnitTest.h"
#include "../LightsOff/Light.cpp"
//#include "../ThirdParty/include/SFML/Graphics.hpp"
//#include <SFML/Graphics.hpp>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTestLightsOff
{
	TEST_CLASS(UnitTestLightsOff)
	{
	public:
		
		TEST_METHOD(TestMethod1)
		{
			Assert::AreEqual(5, 2 + 3);
		}

		TEST_METHOD(DefaultLightConstructor)
		{
			Light object;
			Assert::IsTrue(object.GetState() == Light::State::Off);
		}

		TEST_METHOD(LightSetState)
		{
			Light object;
			object.SetState(Light::State::On);
			Assert::IsTrue(object.GetState() == Light::State::On);
		}

		TEST_METHOD(LightSwitch)
		{
			Light object;
			object.Switch();
			Assert::IsTrue(object.GetState() == Light::State::On);
		}
	};
}